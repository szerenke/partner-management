<?php

namespace App\Http\Services;

use App\Models\Institute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InstitutesService
{
    protected $partnersService;

    public function __construct(PartnersService $partnersService)
    {
        $this->partnersService = $partnersService;
    }

    //List of institutes selected by category for parent institute
    public function instituteCategorySelected(Request $request)
    {
        $category_id = $request->category_id;
        $request->session()->put('category_id', $category_id);

        if($request->session()->get('parent_id') != null) {
            $parent_id = $request->session()->get('parent_id');
        } else {
            $parent_id = 0;
        }

        $query = $this->partnersService->hierarcyqueryByCategory($category_id);
        $institutes = $this->partnersService->institutesPathRecursion($query);

        return view('institutes.parent-selection',compact('institutes', 'parent_id'));
    }

    //Select parent institute for new institute
    public function selectParentInstitute(Request $request)
    {
        $parent_id = $request->parent_id;
        $request->session()->put('parent_id', $parent_id);

        return redirect()->route('institutes.create');
    }

    public function backSelectedParentInstitute(Request $request)
    {
        $parent_id = $request->session()->get('parent_id');
        $category_id = $request->session()->get('category_id');

        $query = $this->partnersService->hierarcyqueryByCategory($category_id);
        $institutes = $this->partnersService->institutesPathRecursion($query);

        return view('institutes.parent-selection',compact('institutes', 'parent_id'));
    }
}
