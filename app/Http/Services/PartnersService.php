<?php

namespace App\Http\Services;

use App\Models\Institute;
use Illuminate\Support\Facades\DB;

class PartnersService
{
    //Query of all specialities of institutes
    public function specialities()
    {
        return Institute::with('specialities')->get();
    }

    //Get the hierarchical path of a selected institute from child to parents, no concatenation!
    public function singlepath($id)
    {
        $query = Institute::where('id', $id)
            ->select('id', 'institute_name', 'parent_id')
            ->unionAll(
                Institute::join('institute_path', 'institute_path.parent_id', '=', 'institutes.id')
                    ->select('institutes.id', 'institutes.institute_name', 'institutes.parent_id')
            );

        return Institute::from('institute_path')
            ->withRecursiveExpression('institute_path', $query)
            ->select('id', 'institute_name', 'parent_id')
            ->get();
    }

    //Hierarchical path of all institutes
    public function hierarcyquery()
    {
        return Institute::whereNull('parent_id')
            ->select('id', 'institute_name', 'institute_name as path', 'category_id')
            ->unionAll(
                Institute::join('institute_path', 'institute_path.id', '=', 'institutes.parent_id')
                    ->select('institutes.id', 'institutes.institute_name', DB::raw("concat (institute_path.path, ' > ', institutes.institute_name)"), 'institutes.category_id')
            );
    }

    //Hierarchical path of institutes selectey by category
    public function hierarcyqueryByCategory($category_id)
    {
        return Institute::whereNull('parent_id')
            ->where('category_id', $category_id)
            ->select('id', 'institute_name', 'institute_name as path')
            ->unionAll(
                Institute::join('institute_path', 'institute_path.id', '=', 'institutes.parent_id')
                    ->select('institutes.id', 'institutes.institute_name', DB::raw("concat (institute_path.path, ' > ', institutes.institute_name)"))
            );
    }

    //Institutes path recursion, orderby instites.institute_name no pagination
    public function institutesPathRecursion($query)
    {
        return Institute::from('institute_path')
            ->withRecursiveExpression('institute_path', $query)
            ->select('id', 'institute_name', 'path')
            ->orderby('institute_path.path')
            ->get();
    }

    //join institutes with path to people orderBy institute asc no pagination
    public function joinInstitutesPeopleNoPagination()
    {
        $query = $this->hierarcyquery();

        return Institute::from('institute_path')
            ->withRecursiveExpression('institute_path', $query)
            ->select('institute_path.id', 'institute_path.institute_name', 'institute_path.path', 'people.*')
            ->rightjoin('people', 'institute_path.id', '=', 'people.institute_id',)
            ->orderBy('institute_path.path')
            ->get();
    }

    //join institutes with path to people orderBy person.id asc with pagination
    public function joinInstitutesPeople($page)
    {
        $query = $this->hierarcyquery();

        return Institute::from('institute_path')
            ->withRecursiveExpression('institute_path', $query)
            ->select('institute_path.id', 'institute_path.institute_name', 'institute_path.path', 'people.*')
            ->rightjoin('people', 'institute_path.id', '=', 'people.institute_id',)
            ->paginate($page);
    }

    //join institutes with path to people orderBy given field and direction with pagination
    public function joinInstitutesPeopleOrdering($page, $field, $direction)
    {
        $query = $this->hierarcyquery();

        return Institute::from('institute_path')
            ->withRecursiveExpression('institute_path', $query)
            ->select('institute_path.id', 'institute_path.institute_name', 'institute_path.path', 'people.*', )
            ->rightjoin('people', 'institute_path.id', '=', 'people.institute_id')
            ->orderBy($field, $direction)
            ->paginate($page);
    }

    //join institutes with path to people orderBy two fields given direction with pagination
    public function joinInstitutesPeopleOrdering2($page, $field1, $field2, $direction)
    {
        $query = $this->hierarcyquery();

        return Institute::from('institute_path')
            ->withRecursiveExpression('institute_path', $query)
            ->select('institute_path.id', 'institute_path.institute_name', 'institute_path.path', 'people.*')
            ->rightjoin('people', 'institute_path.id', '=', 'people.institute_id',)
            ->orderBy($field1, $direction)
            ->orderBy($field2, $direction)
            ->paginate($page);
    }
}
