<?php

namespace App\Http\Services;

use App\Models\Event;
use App\Models\Participant;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class EventsService
{
    //Count number of participants of events
    public function countParticipants()
    {
        $events = Event::all();
        $event_participants = [];

        foreach ($events as $event) {
            $count = $event->participants()->whereHas('eventroles', function (Builder $query) {
                $query->where('eventroles.eventrole_name', 'Eljön');
            })->count();

            array_push($event_participants,
                [ 'event_id' => $event->id,
                    'participants_no' => $count
                ]);
        }
        return $event_participants;
    }

    //Query of all eventroles of participants
    public function eventRoles()
    {
        return Participant::with('eventroles')->get();
    }

    //Choose if new added participant receive an email or not
    public function addParticipantsOption(Request $request)
    {
        $event_id = $request->session()->get('event_id');

        return view('eventemails.participants-option', compact('event_id'));
    }

    //Select the event email the chosen participants will receive
    public function selectEventEmail(Request $request)
    {
        $participants_option = $request->participants_option;
        $event_id = $request->session()->get('event_id');

        $event = Event::findOrFail($event_id);
        $eventemails = $event->eventemails()->get();

        $request->session()->put('participants_option', $participants_option);

        if((int)$participants_option === 1){
            return view('eventemails.select-email', compact('eventemails', 'event_id'));
        } else {
            return redirect()->route('participants.create');
        }
    }

    //After email selected view the create participants form
    public function eventEmailSelected(Request $request)
    {
        $eventemail_id =  $request->mail_id;
        $request->session()->put('eventemail_id', $eventemail_id);

        return redirect()->route('participants.create');
    }
}
