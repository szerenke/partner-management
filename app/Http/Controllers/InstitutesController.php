<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Person;
use App\Models\Institute;
use App\Http\Services\PartnersService;
use App\Models\Speciality;
use Illuminate\Http\Request;

class InstitutesController extends Controller
{
    protected $partnersService;
    protected $page;

    public function __construct(PartnersService $partnersService)
    {
        $this->partnersService = $partnersService;
        $this->page = config('constants.page');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //List institutes hierarchy not included individuals
    public function index(Request $request)
    {
        $request->session()->forget(['category_id', 'parent_id', 'institute_id', 'country_id', 'city_id']);
        $backUrl = $request->getRequestUri();
        $request->session()->put('backUrl', $backUrl);

        $institutes = Institute::where('parent_id',NULL)
            ->whereNotIn('category_id', [4, 6])
            ->orderby('institute_name')
            ->paginate($this->page);

        $institutes_specialities = Institute::with('specialities')->get();

        return view('institutes.index', compact('institutes', 'institutes_specialities'));
    }

    //List institutes by selected category for new person
    public function categoryselectedPerson(Request $request,$id)
    {
        $url = $request->session()->get('url');
        $institute_id = null;

        if($url === '/categories/edit/people/'.$id){
            $person = Person::findOrFail($id);
            $person_id = $person->id;
            if($person->institute_id != null) {
                $institute_id = $person->institute_id;
            }
        } else {
            $person_id = $request->session()->get('person_id');
            $person = Person::findOrFail($person_id);
        }

        $request->session()->put('person_id', $person_id);

        $category_id = $request->category_id;
        $request->session()->put('category_id', $category_id);

        $individual = null;

        if((int)$category_id === 4)
        {
            $institute_name = $person->title . ' ' . $person->lastname . ' ' . $person->firstname;
            $institute = Institute::create([
                'institute_name' => $institute_name,
                'category_id' => $category_id
            ]);
            $institute->save();
            $individual = Institute::findOrFail($institute->id);
            $institutes = null;

        } else {
            $query = $this->partnersService->hierarcyqueryByCategory($category_id);
            $institutes = $this->partnersService->institutesPathRecursion($query);

        }
        if($url === '/categories/index/people/'.$person_id) {
            return view('institutes.select', compact('institutes', 'individual', 'person_id','person'));
        } else {
            return view('institutes.select-edit', compact('institutes', 'individual', 'person', 'person_id', 'institute_id'));
        }
    }

    //Select and save the institute by category for new person
    public function selectedInstitutePerson(Request $request, $id)
    {
        $institute_id = $request->institute_id;
        $person = Person::findOrFail($id);

        if($person->institute_id != null) {
            $personInstitute = Institute::findOrFail($person->institute_id);
            if ((int)$personInstitute->category_id === 4) {
                $personInstitute->delete();
            }
        }
        $person->institute_id = $institute_id;
        $person->save();

        $request->session()->forget(['person_id', 'category_id']);

        return redirect()->route('people.show', $person->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $institute_id = $request->session()->get('institute_id');

        if($institute_id != null)
        {
            $institute = Institute::findOrFail($institute_id);
            return view('institutes.create', compact('institute'));
        } else {
            return view('institutes.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $category_id = $request->session()->get('category_id');
        $institute_id = $request->session()->get('institute_id');

        $request->validate([
            'institute_name' => ['required', 'string', 'max:255'],
            'short_name' => ['string', 'max:255', 'nullable'],
        ]);

        if($institute_id == null){
            $institute = Institute::create([
                'institute_name' => $request->institute_name,
                'short_name' => $request->short_name,
            ]);
        } else{
            $institute = $this->updateNewInstitute($request, $institute_id);
        }

        if($request->session()->get('parent_id') == 0)
        {
            $institute->parent_id = null;
        } else {
            $parent_id = $request->session()->get('parent_id');
            $parent = Institute::findOrFail($parent_id);
            $institute->parent_id = $parent->id;
        }
        $institute->category_id = $category_id;
        $institute->save();

        $institute_id = $institute->id;
        $request->session()->put('institute_id', $institute_id);

        return redirect('specialities');
    }

    //Select specialities and save in institute_speciality table
    public function saveSpecialities(Request $request)
    {
        if($request->input('specialities') != null)
        {
            $specialities = $request->input('specialities');
            $institute_id = $request->session()->get('institute_id');
            $institute = Institute::findOrFail($institute_id);
            $institute->specialities()->detach();

            foreach($specialities as $speciality)
            {
                $institute->specialities()->attach($speciality);
            }
            $institute->save();
        }

        return redirect()->route('addresses.index');
    }

    //Select and store address for institute
    public function saveAddress(Request $request)
    {
        $address_id = $request->address_id;
        $institute_id = $request->session()->get('institute_id');
        $institute = Institute::findOrfail($institute_id );

        $institute->address_id = $address_id;
        $institute->save();

        $request->session()->forget(['category_id', 'parent_id', 'institute_id', 'country_id', 'city_id']);

        return redirect('institutes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $backUrl = $request->session()->get('backUrl');
        $institute = Institute::findOrFail($id);
        $institute->category()->get();

        if($institute->address != null)
        {
            $institute->address->city->country->get();
        }
        $parent_path = $this->partnersService->singlepath($institute->id);
        $specialities = $institute->specialities()->get();

        return view('institutes.show', compact('institute', 'parent_path',  'specialities', 'backUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $institute = Institute::findOrFail($id);

        return view('institutes.edit', compact('institute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'institute_name' => ['required', 'string', 'max:255'],
            'short_name' => ['string', 'max:255', 'nullable'],
        ]);
        $institute = Institute::findOrFail($id);

        $institute->institute_name =  $request->get('institute_name');
        $institute->short_name = $request->get('short_name');

        $institute->save();

        return redirect()->route('institutes.show', $institute->id);
    }

    public function updateNewInstitute($request, $institute_id)
    {
        $institute = Institute::findOrFail($institute_id);

        $institute->institute_name = $request->get('institute_name');
        $institute->short_name = $request->get('short_name');

        return $institute;
    }

    public function editCategory($id)
    {
        $categoriesAll = Category::all();
        $categories = $categoriesAll->whereNotIn('id', [4, 6]);
        $institute = Institute::findOrFail($id);
        $category_id = $institute->category_id;

        return view('categories.edit-for-institute', compact('institute', 'categories', 'category_id'));
    }

    public function updateCategory(Request $request, $id)
    {
        $institute = Institute::findOrFail($id);

        $institute->category_id = $request->category_id;
        $institute->save();

        return redirect()->route('institutes.show', $id);
    }

    public function editSpecialities($id)
    {
        $specialities = Speciality::all();

        $institute = Institute::findOrFail($id);
        $institute_specialities = $institute->specialities()->get();

        return view('specialities.edit', compact('institute', 'specialities', 'institute_specialities'));
    }

    public function updateSpecialities(Request $request, $id)
    {
        $institute = Institute::findOrFail($id);
        $institute->specialities()->detach();

        if($request->input('specialities') != null)
        {
            $specialities = $request->input('specialities');

            foreach($specialities as $speciality)
            {
                $institute->specialities()->attach($speciality);
            }
            $institute->save();
        };

        return redirect()->route('institutes.show', $id);
    }

    //Select and store address for institute
    public function editAddress($id)
    {
        $institute = Institute::findOrFail($id);

        return view('addresses.edit', compact('institute'));
    }

    public function updateAddress(Request $request, $id)
    {
        $institute = Institute::findOrfail($id );

        $institute->address_id = $request->address_id;
        $institute->save();

        return redirect()->route('institutes.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $institute = Institute::findOrFail($id);
        $countChild = $institute->childinstitutes()->count();
        $countPeople = $institute->people()->count();

        if($countChild === 0 || $countPeople === 0){
            $institute->delete();
            return redirect('/institutes');
        } else {
            return redirect('/institutes')->with('status', 'Felettes intézmény, ill. partnerekhez tartozó intézmény nem törölhető!');
        }
    }
}
