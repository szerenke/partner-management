<?php

namespace App\Http\Controllers;

use App\Models\Institute;
use App\Models\Person;
use App\Http\Services\PartnersService;
use Illuminate\Http\Request;

class PeopleController extends Controller
{
    protected $partnersService;
    protected $page;

    public function __construct(PartnersService $partnersService)
    {
        $this->partnersService = $partnersService;
        $this->page = config('constants.page');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->session()->forget(['person_id', 'category_id']);

        $people = $this->partnersService->joinInstitutesPeople($this->page);
        $institutes_specialities = Institute::with('specialities')->get();

        return view('people.index', compact('people', 'institutes_specialities'));
    }

    public function indexDesc()
    {
        $people = $this->partnersService->joinInstitutesPeopleOrdering($this->page, 'people.id', 'desc');

        $institutes_specialities = Institute::with('specialities')->get();

        return view('people.index', compact('people', 'institutes_specialities'));
    }

    public function sortByLastname()
    {
        $people = $this->partnersService->joinInstitutesPeopleOrdering2($this->page,  'people.lastname', 'people.firstname', 'asc');
        $institutes_specialities = Institute::with('specialities')->get();

        return view('people.index', compact('people', 'institutes_specialities'));
    }

    public function sortByLastnameDesc()
    {
        $people = $this->partnersService->joinInstitutesPeopleOrdering2($this->page,  'people.lastname', 'people.firstname', 'desc');
        $institutes_specialities = Institute::with('specialities')->get();

        return view('people.index', compact('people', 'institutes_specialities'));
    }

    public function sortByPosition()
    {
        $people = $this->partnersService->joinInstitutesPeopleOrdering($this->page, 'people.position', 'asc');
        $institutes_specialities = Institute::with('specialities')->get();

        return view('people.index', compact('people', 'institutes_specialities'));
    }

    public function sortByPositionDesc()
    {
        $people = $this->partnersService->joinInstitutesPeopleOrdering($this->page, 'people.position', 'desc');
        $institutes_specialities = Institute::with('specialities')->get();

        return view('people.index', compact('people', 'institutes_specialities'));
    }

    public function sortByInstitute()
    {
        $people = $this->partnersService->joinInstitutesPeopleOrdering($this->page, 'institute_path.path', 'asc');
        $institutes_specialities = Institute::with('specialities')->get();

        return view('people.index', compact('people', 'institutes_specialities'));
    }

    public function sortByInstituteDesc()
    {
        $people = $this->partnersService->joinInstitutesPeopleOrdering($this->page, 'institute_path.path', 'desc');
        $institutes_specialities = Institute::with('specialities')->get();

        return view('people.index', compact('people', 'institutes_specialities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $backUrl = $request->session()->get('backUrl');
        $person_id = $request->session()->get('person_id');

        if($backUrl === '/people/'.$person_id){
           $request->session()->forget('person_id');
        }

        if($person_id != null && $backUrl != '/people/'.$person_id){
            $person = Person::findOrfail($person_id);
            return view('people.create', compact('person'));
        } else {
            return view('people.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function validatePerson(Request $request)
    {
        $request->validate([
            'lastname' => ['required', 'string', 'max:255'],
            'firstname' => ['required', 'string', 'max:255'],
            'title' => ['string', 'max:10', 'nullable'],
            'position' => ['string', 'max:50', 'nullable'],
            'description' => ['string', 'max:500', 'nullable']
        ]);
    }

    public function store(Request $request)
    {
        $this->validatePerson($request);

        $person_id = $request->session()->get('person_id');
        $manager = $request->manager_id;

        if($person_id === null) {

            $person = Person::create([
                'lastname' => $request->lastname,
                'firstname' => $request->firstname,
                'title' => $request->title,
                'position' => $request->position,
                'description' => $request->description,
                'gender' => $request->gender
            ]);
        } else {
                $person = Person::findOrFail($person_id);
                $this->personUpdate($request, $person);
            }

        if ((int)$manager === 1) {
            $person->manager_id = $person->id;
            } else {
            $person->manager_id = null;
        }
        $person->save();
        $request->session()->put('person_id', $person->id);

        return redirect()->route('mails.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $backUrl = $request->getRequestUri();
        $request->session()->put('backUrl', $backUrl);

        $person = Person::findOrFail($id);

        $gender = ($person->gender == 'male') ? 'férfi' : 'nő';
        $manager = ($person->manager_id == $person->id) ? 'igen' : 'nem';

         if($person->institute_id != null) {
             $institute = Institute::findOrFail($person->institute_id);
             $parent_path = $this->partnersService->singlepath($institute->id);
             $specialities = $institute->specialities()->get();
            return view('people.show', compact('person', 'gender', 'manager', 'specialities', 'parent_path'));
         }
        else {
            return view('people.show', compact('person', 'gender', 'manager'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $person = Person::findOrFail($id);

        return view('people.edit', compact('person'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function personUpdate(Request $request, $person)
    {
        $this->validatePerson($request);

        $person->lastname =  $request->get('lastname');
        $person->firstname = $request->get('firstname');
        $person->title = $request->get('title');
        $person->position = $request->get('position');
        $person->description = $request->get('description');
        $person->gender = $request->get('gender');

        return $person;
    }

    public function update(Request $request, $id)
    {
        $person = Person::findOrFail($id);
        $manager = $request->manager_id;
        $this->personUpdate($request, $person);

        if($manager == 1) {
            $person->manager_id = $person->id;
        } else {
            $person->manager_id = null;
        }
        $person->save();
        return redirect()->route('people.show', $person->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $person = Person::findOrFail($id);
        $this->deletePersonIndividualInstitute($id);
        $person->delete();
        return redirect('/people');
    }

    public function deletePersonIndividualInstitute($id)
    {
        $person = Person::findOrFail($id);
        $personInstitute = Institute::findOrFail($person->institute_id);

        if((int)$personInstitute->category_id === 4)
        {
            $personInstitute->delete();
        }
    }
}
