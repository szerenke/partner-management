<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Institute;
use App\Models\Person;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->session()->get('category_id') != null) {
            $category_id = $request->session()->get('category_id');
        } else {
            $category_id = 0;
        }
        $categoriesAll = Category::all();
        $categories = $categoriesAll->whereNotIn('id', [4, 6]);

        return view('categories.institute-category', compact( 'categories', 'category_id'));
    }

    public function indexPeople(Request $request)
    {
        $url = $request->getRequestUri();
        $request->session()->put('url', $url);

        $categories = Category::all();

        if($request->session()->get('category_id') != null) {
            $category_id = $request->session()->get('category_id');
        } else {
            $category_id = 0;
        }

        $person_id = $request->session()->get('person_id');
        return view('categories.select', compact('categories', 'category_id', 'person_id'));
    }

    public function editPeople(Request $request, $id)
    {
        $url = $request->getRequestUri();
        $request->session()->flash('url', $url);

        $categories = Category::all();
        $person = Person::findOrFail($id);

        if($person->institute_id != null) {
            $institute = Institute::findOrFail($person->institute_id);
            $category_id = $institute->category_id;

        } else {
            $category_id = 0;
        }

        $request->session()->flash('category_id', $category_id);

        return view('categories.edit-for-person', compact('categories', 'category_id', 'person'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uri = $request->getRequestUri();

        $request->validate([
            'category_name' => ['required', 'string', 'max:100'],
        ]);

        $category = Category::create([
            'category_name' => $request->category_name,
        ]);

        $category->save();

        return redirect($uri);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
