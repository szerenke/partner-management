<?php

namespace App\Http\Controllers;

use App\Exports\EventsExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Facades\Excel;

class EventsExportController extends Controller
{
    use Exportable;

    public function export()
    {
        return Excel::download(new EventsExport, 'events.xlsx');
    }
}
