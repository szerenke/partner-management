<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Facades\Excel;

class UsersExportController extends Controller
{
    use Exportable;

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
