<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Eventemail;
use Illuminate\Http\Request;

class EventemailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $event_id = $request->session()->get('event_id');

        $event = Event::findOrFail($event_id);
        $event_name = $event->event_name;

        return view('eventemails.create', compact('event_name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function validateEventEmail(Request $request)
    {
        $request->validate([
            'sender' => ['required', 'string', 'email', 'max:255'],
            'email_subject' => ['required', 'string', 'max:255'],
            'email_message' => ['string', 'max:500', 'nullable'],
            'signature' => ['required', 'string', 'max:255'],
            'event_link' => ['string', 'max:255', 'active_url', 'nullable']
        ]);
    }

    public function store(Request $request)
    {
        $event_id = $request->session()->get('event_id');

        $this->validateEventEmail($request);

        $eventemail = Eventemail::create([
            'sender' => $request->sender,
            'email_subject' => $request->email_subject,
            'email_message' => $request->email_message,
            'signature' => $request->signature,
            'event_link' => $request->event_link,
            'event_id' => $event_id
        ]);

        $eventemail->save();

        return redirect()->route('eventemails.show', $eventemail->event_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $eventemails = Eventemail::where('event_id', $id)->get();
        $event = Event::findOrFail($id);

        $i = 1;

        return view('eventemails.show', compact('eventemails', 'event', 'i'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventemail = Eventemail::findOrFail($id);
        $event = Event::findOrFail($eventemail->event_id);
        $event_name = $event->event_name;

        return view('eventemails.edit', compact('eventemail', 'event_name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateEventEmail($request);

        $eventemail = Eventemail::findOrfail($id);

        $eventemail->sender = $request->get('sender');
        $eventemail->email_subject = $request->get('email_subject');
        $eventemail->email_message = $request->get('email_message');
        $eventemail->signature = $request->get('signature');
        $eventemail->event_link = $request->get('event_link');

        $eventemail->save();

        return redirect()->route('eventemails.show', $eventemail->event_id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eventemail = Eventemail::findOrFail($id);
        $event = Event::findOrFail($eventemail->event_id);

        if($eventemail->sent_date === null){
            $eventemail->delete();
            return redirect()->route('eventemails.show', $event->id);
        } else {
            return redirect()->route('eventemails.show', $event->id)->with('status', 'Olyan esemény e-mail, ami már el lett küldve nem törölhető!');
        }
    }
}
