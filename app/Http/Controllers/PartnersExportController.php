<?php

namespace App\Http\Controllers;

use App\Http\Services\PartnersService;
use App\Exports\PartnersExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Facades\Excel;

class PartnersExportController extends Controller
{
    use Exportable;

    public function export(PartnersService $partnersService)
    {
        return Excel::download(new PartnersExport($partnersService), 'people.xlsx');
    }
}
