<?php

namespace App\Http\Controllers;

use App\Events\NewEventInvitationEmailEvent;
use App\Models\Event;
use App\Models\Eventemail;
use App\Models\Eventrole;
use App\Models\Participant;
use App\Models\Person;
use Illuminate\Http\Request;

class ParticipantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $event_id = $request->session()->get('event_id');
        $event = Event::findOrFail($event_id);

        return view('participants.create', compact('event'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $eventemail_id = $request->session()->get('eventemail_id');
        $event_id = $request->session()->get('event_id');
        $invitation_event = Event::findOrFail($event_id);

        $participants_option = $request->session()->get('participants_option');

        if($request->input('people') != null)
        {
            $people_id = $request->input('people');

            foreach ($people_id as $person_id)
            {
                $participant = Participant::create([
                    'person_id' => $person_id,
                    'event_id' => $event_id
                ]);
                if((int)$participants_option === 1){
                    $eventrole_id = 1;
                }
                if((int)$participants_option === 0) {
                    $eventrole_id = 2;
                }

                $participant->eventroles()->attach($eventrole_id);
                $participant->save();

            }
            //!!! Must be uncommented when send invitation e-mails
            if((int)$participants_option === 1) {
                event(new NewEventInvitationEmailEvent($people_id, $invitation_event, $eventemail_id));

                $eventemail = Eventemail::findOrFail($eventemail_id);
                $eventemail->sent_date = now();
                $eventemail->save();
            }
        }
        $request->session()->forget(['event_id', 'participants_option', 'eventemail_id']);

        return redirect()->route('events.show', $event_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $event_id = $request->session()->get('event_id');
        $person_id = $id;

        $eventroles = Eventrole::all();
        $presenterRole = $eventroles->where('eventrole_name', '=', 'Előadó')->first();

        $event = Event::findOrFail($event_id);
        $person = Person::findOrFail($person_id );

        $participant = Participant::where('person_id', $person_id)
                            ->where('event_id', $event_id)->first();

        $request->session()->put('participant', $participant);
        $participant_roles = $participant->eventroles()->get();

        if(!is_null($event->presentations()->where('person_id', $person_id)->first())) {
            $presentation =  $event->presentations()->where('person_id', $person_id)->first();
            if(!is_null($presentation->speciality_id)) {
                $speciality = $presentation->speciality->speciality_name;
            } else {
                $speciality = '';
            }
        } else {
            $presentation = null; $speciality = '';
        }

        return view('participants.edit', compact('person', 'event', 'participant_roles', 'eventroles', 'presenterRole', 'presentation', 'speciality'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateRoles(Request $request)
    {
        if($request->input('eventroles') != null)
        {
            $eventroles = $request->input('eventroles');
            $participant = $request->session()->get('participant');

            $participant->eventroles()->detach();

            foreach ($eventroles as $role) {
                $participant->eventroles()->attach($role);
                $participant->save();
            }
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
