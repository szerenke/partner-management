<?php

namespace App\Http\Controllers;

use App\Http\Services\EventsService;
use App\Models\Event;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    protected $eventsService;

    public function __construct(EventsService $eventsService)
    {
        $this->eventsService = $eventsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event_participants = $this->eventsService->countParticipants();

        return view('events.index', compact('event_participants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function validateEvent(Request $request)
    {
        $request->validate([
            'event_name' => ['required', 'string', 'max:255'],
            'event_description' => ['string', 'max:500', 'nullable'],
            'event_location' => ['string', 'max:255', 'nullable'],
            'start_date' => ['required', 'date', 'after:tomorrow'],
            'end_date' => ['required', 'date', 'after:start_date'],
        ]);
    }

    public function store(Request $request)
    {
        $this->validateEvent($request);

        $event = Event::create([
            'event_name' => $request->event_name,
            'event_description' => $request->event_description,
            'event_location' => $request->event_location,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ]);

        $event->save();

        return redirect()->route('events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        $participant_roles = $this->eventsService->eventRoles();
        $request->session()->put('event_id', $id);

        return view('events.show', compact('event',  'participant_roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        return view('events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateEvent($request);

        $event = Event::findOrfail($id);

        $event->event_name = $request->get('event_name');
        $event->event_description = $request->get('event_description');
        $event->event_location = $request->get('event_location');
        $event->start_date = $request->get('start_date');
        $event->end_date = $request->get('end_date');

        $event->save();

        return redirect()->route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);

        if($event->start_date > date("Y-m-d H:i:s") || $event->participants()->count() === 0) {
            $event->delete();
            return redirect('/events');
        } else {
            return redirect()->route('events.show', $event->id)->with('status', 'Olyan esemény, ami lezajlott, vagy vannak meghívottak nem törölhető!');
        }
    }
}
