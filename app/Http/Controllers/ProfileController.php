<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Password;

class ProfileController extends Controller
{
    protected $usersController;

    public function __construct(UsersController $usersController)
    {
        $this->usersController = $usersController;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        return view('profile.edit', compact('user'));
    }

    public function editPassword($id)
    {
        $user = Auth::user();
        return view('profile.edit-password', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = User::all();
        $user = Auth::user();

        if((int)$id === $user->id) {
            $user = $this->usersController->validateUserUpdate($request, $user);

            if($request->get('email') != $user->email && $users->contains('email', $request->get('email'))){
                return back()->withErrors(['email' => 'Az email cím már foglalt']);
            } else {
                $user->email = $request->get('email');
            }

            $user->save();
            return redirect('dashboard/profile/myprofile');
        } else {
            return redirect('dashboard/profile/myprofile')->with('status', 'Csak saját profil szerkeszthető itt');
        }
    }

    public function updatePassword(Request $request, $id)
    {
        $user = Auth::user();

        if((int)$id === $user->id) {
            $request->validate([
                'current_password' => 'required',
                //'password' => ['required', 'same:confirm_password', Rules\Password::defaults()],
                'password' => ['required', 'same:confirm_password', Password::min(8)
                    ->letters()
                    ->mixedCase()
                    ->numbers()
                    ->symbols()],
                'confirm_password' => 'required',
            ]);

            if (!Hash::check($request->current_password, $user->password)) {
                return back()->withErrors(['current_password' => 'Hibás jelszó']);
            }
            $user->password = Hash::make($request->password);
            $user->remember_token = Str::random(10);
            $user->save();

        return redirect('dashboard/profile/myprofile');

        } else {
            return redirect('dashboard/profile/myprofile')->with('status', 'Csak saját jelszó módosítható');
        }
    }
}
