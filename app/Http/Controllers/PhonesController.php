<?php

namespace App\Http\Controllers;

use App\Models\Phone;
use App\Models\Person;
use Illuminate\Http\Request;

class PhonesController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $url = $request->getRequestUri();
        $request->session()->put('url', $url);

        $person_id = $request->session()->get('person_id');
        $person = Person::findOrFail($person_id);

        $i = 0;

        return view('phones.create', compact('person', 'i'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $url = $request->session()->get('url');
        $person_id = $request->get('person_id');

        if($request->phone_number != null) {
            $request->validate([
                'phone_number' => ['string', 'max:255', 'nullable']
            ]);

            $phone = Phone::create([
                'phone_number' => $request->phone_number,
                'person_id' => $person_id
            ]);
            $phone->save();
        }

        if($url != '/phones/create')
        {
            return redirect()->route('phones.edit', $person_id);
        }
        return redirect()->route('phones.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $url = $request->getRequestUri();
        $request->session()->put('url', $url);

        $person = Person::findOrFail($id);
        $person->phones()->get();
        $i = 0;

        return view('phones.edit', compact('person', 'i'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $url = $request->session()->get('url');

        $phone = Phone::findOrFail($id);
        $phones = Phone::all();

        $index = $request->get('index');
        $phonenumber = $request->get('phone_number'.$index);

        $request->validate([
            'phone_number'.$index => ['required', 'string', 'max:255']
        ]);

        if($phonenumber != $phone->phone_number && $phones->contains('phone_number', $phonenumber)){
            return back()->withErrors(['phone_number'.$index => 'A telefonszám már foglalt']);
        } else {
            $phone->phone_number = $phonenumber;
        }
        $phone->save();

        if($url != '/phones/create')
        {
            return redirect()->route('phones.edit', $phone->person_id);
        }
        return redirect()->route('phones.create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $url = $request->session()->get('url');
        $phone = Phone::findOrFail($id);
        $person_id = $phone->person_id;
        $phone->delete();

        if($url != '/phones/create')
        {
            return redirect()->route('phones.edit', $person_id);
        }
        return redirect()->route('phones.create');
    }
}
