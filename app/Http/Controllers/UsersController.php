<?php

namespace App\Http\Controllers;

use App\Events\RegisteredNewUserEvent;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;

class UsersController extends Controller
{
    protected $page;

    public function __construct()
    {
        $this->page = config('constants.page');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('roles')->paginate($this->page);

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'lastname' => ['required', 'string', 'max:255'],
            'firstname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'lastname' => $request->lastname,
            'firstname' => $request->firstname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $user->attachRole($request->role_id);
        $user->remember_token = Str::random(10);
        $user->email_verified_at = now();
        $user->save();

        event(new RegisteredNewUserEvent($user));

        return redirect('/users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $role = $user->roles->first()->name;

        if($user->id===Auth::user()->id){
            return redirect('/users')->with('success', 'Saját adatok szerkesztése a profilban!');

        } else {
            return view('users.edit', compact('user', 'role'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //reuse this validation update function at profile
    public function validateUserUpdate(Request $request, $user)
    {
        $request->validate([
            'lastname' => ['required', 'string', 'max:255'],
            'firstname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        $user->lastname =  $request->get('lastname');
        $user->firstname = $request->get('firstname');

        return $user;
    }

    public function update(Request $request, $id)
    {
        $users = User::all();
        $user = User::findOrFail($id);
        $user = $this->validateUserUpdate($request, $user);

        if($request->get('email') != $user->email && $users->contains('email', $request->get('email'))){
            return back()->withErrors(['email' => 'Az email cím már foglalt']);
        } else {
            $user->email = $request->get('email');
        }

        $user->detachRole('admin');
        $user->detachRole('user');
        $user->attachRole($request->role_id);
        $user->save();

        return redirect('/users')->with('success', 'User updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if($user->id === Auth::user()->id){
        } else {
            $user->delete();
        }
        return redirect('/users');
    }
}
