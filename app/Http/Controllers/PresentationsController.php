<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Person;
use App\Models\Presentation;
use App\Models\Speciality;
use Illuminate\Http\Request;

class PresentationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $event_id = $request->session()->get('event_id');
        $participant = $request->session()->get('participant');
        $person_id = $participant->person_id;

        $event = Event::findOrfail($event_id);
        $person = Person::findOrfail($person_id);

        $specialities = Speciality::all();

        return view('presentations.create', compact('event', 'person', 'specialities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $participant = $request->session()->get('participant');

        $request->validate([
            'presentation_title' => ['required', 'string', 'max:255'],
            'presentation_description' => ['string', 'max:500', 'nullable'],
        ]);

        $presentation = Presentation::create([
            'presentation_title' => $request->presentation_title,
            'presentation_description' => $request->presentation_description,
            'event_id' => $participant->event_id,
            'person_id' => $participant->person_id,
            'speciality_id' => $request->speciality
        ]);

        $presentation->save();

        return redirect()->route('events.show', $participant->event_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $presentations = Presentation::where('event_id', $id)->get();
        $event = Event::findOrFail($id);
        $i = 1;

        return view('presentations.show', compact('presentations', 'event', 'i'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $presentation = Presentation::findOrFail($id);
        $event = Event::findOrfail($presentation->event_id);

        if($presentation->person_id != null) {
            $person = Person::find($presentation->person_id);
        } else {
            $person = null;
        }

        if($presentation->speciality_id != null) {
            $speciality_id =  $presentation->speciality_id;
        } else {
            $speciality_id = 0;
        }

        $specialities = Speciality::all();

        return view('presentations.edit', compact('event', 'person', 'specialities', 'speciality_id', 'presentation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $presentation = Presentation::findOrFail($id);
        $request->validate([
            'presentation_title' => ['required', 'string', 'max:255'],
            'presentation_description' => ['string', 'max:500', 'nullable'],
        ]);

        $presentation->presentation_title = $request->get('presentation_title');
        $presentation->presentation_description = $request->get('presentation_description');
        $presentation->speciality_id = $request->get('speciality');

        $presentation->save();
        return redirect()->route('events.show', $presentation->event_id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $presentation = Presentation::findOrfail($id);
        $event = Event::findOrfail($presentation->event_id);

        if($event->start_date > date("Y-m-d H:i:s")) {
            $presentation->delete();
            return redirect()->route('events.show', $event->id);
        } else {
            return redirect()->route('events.show', $event->id)->with('status', 'Már lezajlott esemény előadása nem törölhető!');
        }

    }
}
