<?php

namespace App\Http\Controllers;

use App\Models\Email;
use App\Models\Person;
use Illuminate\Http\Request;

class EmailsController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $url = $request->getRequestUri();
        $request->session()->put('url', $url);

        $person_id = $request->session()->get('person_id');
        $person = Person::findOrFail($person_id);
        $person->emails()->get();
        $i = 0;

        return view('mails.create', compact('person', 'i'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $url = $request->session()->get('url');
        $person_id = $request->get('person_id');

        if($request->email_address != null) {
            $request->validate([
                'email_address' => ['string', 'email', 'max:255', 'nullable']
            ]);

            $email = Email::create([
                'email_address' => $request->email_address,
                'person_id' => $person_id
            ]);
            $email->save();
        }

        if($url != '/mails/create')
        {
            return redirect()->route('mails.edit', $person_id);
        }
        return redirect()->route('mails.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $url = $request->getRequestUri();
        $request->session()->put('url', $url);

        $person = Person::findOrFail($id);
        $person->emails()->get();
        $i = 0;

        return view('mails.edit', compact('person', 'i'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $url = $request->session()->get('url');

        $email = Email::findOrFail($id);
        $emails = Email::all();

        $index = $request->get('index');
        $emailaddress = $request->get('email_address'.$index);

        $request->validate([
            'email_address'.$index => ['required', 'string', 'email', 'max:255']
        ]);

        if($emailaddress != $email->email_address && $emails->contains('email_address', $emailaddress)){
            return back()->withErrors(['email_address'.$index => 'Az e-mail cím már foglalt']);
        } else {
            $email->email_address = $emailaddress;
        }
        $email->save();

        if($url != '/mails/create')
        {
            return redirect()->route('mails.edit', $email->person_id);
        }
        return redirect()->route('mails.create');
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $url = $request->session()->get('url');
        $email = Email::findOrFail($id);
        $person_id = $email->person_id;
        $email->delete();

        if($url != '/mails/create')
        {
            return redirect()->route('mails.edit', $person_id);
        }

        return redirect()->route('mails.create');

    }
}
