<?php

namespace App\Http\Controllers;

use App\Models\Event;

class DashboardController extends Controller
{
    public function index()
    {
        $events = Event::where('start_date', '>', now())->orderBy('start_date')->get();

        return view('dashboard', compact('events'));
    }

    public function myprofile()
    {
        return view('profile/myprofile');
    }
}
