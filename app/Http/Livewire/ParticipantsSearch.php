<?php

namespace App\Http\Livewire;

use App\Models\Institute;
use App\Models\Participant;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class ParticipantsSearch extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $pageRecords = 0;

    public $searchText = '';
    public $event;

    public $participant_roles;
    public $institutes_specialities;
    public $countParticipants;
    public $countPresentations;
    public $countEventEmails;

    public function mount()
    {
        $this->institutes_specialities = Institute::with('specialities')->get();
        $this->countParticipants = Participant::where('event_id', $this->event->id)->count();
        $this->countPresentations = $this->event->presentations()->count();
        $this->countEventEmails = $this->event->eventemails()->count();
        $this->pageRecords = config('constants.page');
    }

    public function render()
    {
        $query = $this->hierarcyquery();
        $participants = Participant::where('event_id', $this->event->id)->get();

        return view('livewire.participants-search', [
            'people' => Institute::from('institute_path')
                ->withRecursiveExpression('institute_path', $query)
                ->select('institute_path.id', 'institute_path.category_id', 'institute_path.institute_name', 'institute_path.path', 'people.*')
                ->rightjoin('people', 'institute_path.id', '=', 'people.institute_id',)
                ->whereIn('people.id', $participants->pluck('person_id'))
                ->sharedLock()
                ->search('people.lastname', $this->searchText)
                ->paginate($this->pageRecords),
        ]);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hierarcyquery()
    {
        return Institute::whereNull('parent_id')
            ->select('id', 'institute_name', 'institute_name as path', 'category_id')
            ->unionAll(
                Institute::join('institute_path', 'institute_path.id', '=', 'institutes.parent_id')
                    ->select('institutes.id', 'institutes.institute_name', DB::raw("concat (institute_path.path, ' > ', institutes.institute_name)"), 'institutes.category_id')
            );
    }
}
