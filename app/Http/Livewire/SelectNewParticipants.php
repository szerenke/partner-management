<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Institute;
use App\Models\Participant;
use App\Models\Speciality;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class SelectNewParticipants extends Component
{
    public $people;
    public $event;

    public $instituteCategories;
    public $specialities;

    public $selectedCategory = null;
    public $position;
    public $selectedInstitutes;
    public $selectedSpecialities = [];

    public $checked = false;
    public $checkedAll = '';
    public $unselected;
    public $count = 0;

    public function render()
    {
        $this->people = $this->joinInstitutesPeopleNotEventInvited($this->event->id);
        $this->specialities = Speciality::all();
        $this->instituteCategories = Category::all();

        if($this->selectedCategory != 0) {
            $this->people = $this->people->where('category_id',$this->selectedCategory);
        }

        if($this->position != 0){
            if((int)$this->position === 2) {
                $this->people = $this->people->where('manager_id', null);
            } else {
                $this->people = $this->people->whereNotNull('manager_id');
            }
        }

        if($this->selectedSpecialities != null) {
            $instituteSpecialities = Institute::whereHas('specialities', function (Builder $query) {
                $query->whereIn('speciality_id', $this->selectedSpecialities);
            })
                ->whereIn('id', $this->people->pluck('institute_id'))
                ->get();
            $this->people = $this->people->whereIn('institute_id', $instituteSpecialities->pluck('id'));
        }

        return view('livewire.select-new-participants', [
            'instituteCategories' => $this->instituteCategories,
            'specialities' => $this->specialities,
            'people' => $this->people,
            'checkedAll' => $this->checkedAll
        ]);
    }

    public function updatedPosition($value)
    {
        $this->position = $value;
    }

    public function updatedSelectedCategory($category)
    {
        $this->selectedCategory = $category;
    }

    public function clickAll()
    {
        $this->count++;

        if($this->count % 2 != 0) {
            $this->checked = true;
            $this->checkedAll = 'checked';
        } else if ($this->count === 0) {
            $this->checked = true;
            $this->checkedAll = 'checked';
        } else {
            $this->checked = false;
            $this->checkedAll = '';
        }
    }

    public function unselected()
    {
        $this->checked = false;
        $this->count = 0;
    }

    //Hierarchical path of all institutes
    public function hierarcyquery()
    {
        return Institute::whereNull('parent_id')
            ->select('id', 'institute_name', 'institute_name as path', 'category_id')
            ->unionAll(
                Institute::join('institute_path', 'institute_path.id', '=', 'institutes.parent_id')
                    ->select('institutes.id', 'institutes.institute_name', DB::raw("concat (institute_path.path, ' > ', institutes.institute_name)"), 'institutes.category_id')
            );
    }

   //Recursion part of Not invited people for invitation selection
   public function joinInstitutesPeopleNotEventInvited($event_id)
    {
        $query = $this->hierarcyquery();
        $participants = Participant::where('event_id', $event_id)->get();

        return Institute::from('institute_path')
            ->withRecursiveExpression('institute_path', $query)
            ->select('institute_path.id', 'institute_path.category_id', 'institute_path.institute_name', 'institute_path.path', 'people.*')
            ->rightjoin('people', 'institute_path.id', '=', 'people.institute_id',)
            ->whereNotIn('people.id', $participants->pluck('person_id'))
            ->sharedLock()
            ->get();
    }
}
