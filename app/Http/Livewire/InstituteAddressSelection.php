<?php

namespace App\Http\Livewire;

use App\Models\Address;
use App\Models\City;
use App\Models\Country;
use App\Models\Institute;
use Livewire\Component;

class InstituteAddressSelection extends Component
{
    public $institute;

    public $countries;
    public $cities;
    public $addresses;

    public $selectedCountry = null;
    public $selectedCity = null;
    public $selectedAddress = null;

    public $oldCountry = '';
    public $oldCity = '';
    public $oldAddress = '';

    public function mount($selectedAddress = null)
    {
        if(!is_null($this->institute)) {
            $this->institute = Institute::findOrFail($this->institute->id);

            if(isset($this->institute->address_id)) {
                $this->selectedCountry = $this->institute->address->city->country_id;
                $this->selectedCity = $this->institute->address->city_id;
                $this->selectedAddress = $this->institute->address->id;

                $this->oldCountry = $this->institute->address->city->country_id;
                $this->oldCity = $this->institute->address->city_id;
                $this->oldAddress = $this->institute->address->id;
            }

            $this->countries = Country::all();
            $this->cities = City::where('country_id', $this->selectedCountry)->get();
            $this->addresses = Address::where('city_id', $this->selectedCity)->get();


        } else {
            $this->countries = Country::all();
            $this->cities = collect();
            $this->addresses = collect();
            $this->selectedAddress = $selectedAddress;
        }

        if(!is_null($selectedAddress)) {
            $address = Address::with('city.country')->find($selectedAddress);
            if($address) {
                $this->addresses = Address::where('city_id', $address->city_id)->get();
                $this->cities = City::where('country_id', $address->city->country_id)->get();
                $this->selectedCountry = $address->city->coutry_id;
                $this->selectedCity = $address->city_id;
            }
        }
    }

    public function render()
    {
        return view('livewire.institute-address-selection', [
            'oldCountry' => $this->oldCountry,
            'oldCity' => $this->oldCity,
            'oldAddress' => $this->oldAddress
        ]);
    }

    public function updatedSelectedCountry($val)
    {
        $this->cities = City::where('country_id', $val)->get();
        $this->selectedCity = null;
    }

    public function updatedSelectedCity($val)
    {
        if (!is_null($val)) {
            $this->addresses = Address::where('city_id', $val)->get();
        }
    }
}
