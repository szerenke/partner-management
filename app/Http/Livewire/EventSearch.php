<?php

namespace App\Http\Livewire;

use App\Models\Event;
use Livewire\Component;
use Livewire\WithPagination;

class EventSearch extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $pageRecords = 0;

    public $searchText = '';
    public $event_participants; //this will assign the parameters passed from controller

    public $minYears;
    public $maxYears;

    public $selectedMin = null;
    public $selectedMax = null;

    public $searchFrom = null;
    public $searchTo = null;

    public $defaultMax;

    public function mount()
    {
        $this->pageRecords = config('constants.page');
    }

    public function render()
    {
        $min = date('Y', strtotime(Event::min('start_date')));
        $max = date('Y', strtotime(Event::max('start_date')));

        $this->defaultMax = date('Y-m-d H:i:s', strtotime(Event::max('start_date')));

        if(!is_null($this->searchTo)) {
            $max = date('Y',strtotime($this->searchTo));
        }
        $this->minYears = range($min, $max, 1);

        if(!is_null($this->searchFrom)) {
            $min = date('Y',strtotime($this->searchFrom));
        }
        $this->maxYears = range($max, $min, 1);

        if(is_null($this->searchTo))
        {
            $this->searchTo = $this->defaultMax;
        }

        return view('livewire.event-search', [
            'events' => Event::search('event_name', trim($this->searchText))
                ->whereDate('start_date', '>', (string)$this->searchFrom)
                ->whereDate('start_date', '<', (string)$this->searchTo)
                ->orderBy('start_date', 'desc')
                ->paginate($this->pageRecords)
        ]);
    }

    public function updatedSelectedMin($minYear)
    {
        $this->searchFrom = $minYear . '-01-01 00:00:00';
    }

    public function updatedSelectedMax($maxYear)
    {
        $this->searchTo = $maxYear. '-12-31 23:59:00';
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }
}

