<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EventMessages extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $gender;
    public $salutation;
    public $invitation_event;
    public $eventemail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($gender, $salutation, $invitation_event, $eventemail)
    {
        $this->gender = $gender;
        $this->salutation = $salutation;
        $this->invitation_event = $invitation_event;
        $this->eventemail = $eventemail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->eventemail->subject)
                    ->from($this->eventemail->sender)
                    ->markdown('emails.event-messages');
    }
}
