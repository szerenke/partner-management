<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewEventInvitationEmailEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $people_id;
    public $invitation_event;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($people_id, $invitation_event, $eventemail_id)
    {
        $this->people_id = $people_id;
        $this->invitation_event = $invitation_event;
        $this->eventemail_id = $eventemail_id;
    }

}
