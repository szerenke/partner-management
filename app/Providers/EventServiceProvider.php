<?php

namespace App\Providers;

use App\Events\NewEventInvitationEmailEvent;
use App\Events\RegisteredNewUserEvent;
use App\Listeners\InformNewUserListener;
use App\Listeners\NewEventInviteParticipantsListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        RegisteredNewUserEvent::class => [
            InformNewUserListener::class,
        ],
        NewEventInvitationEmailEvent::class => [
            NewEventInviteParticipantsListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
