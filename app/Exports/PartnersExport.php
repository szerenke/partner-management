<?php

namespace App\Exports;

use App\Http\Services\PartnersService;
use App\Models\Institute;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PartnersExport implements FromCollection, ShouldAutoSize, WithMapping, WithHeadings
{
    protected $partnersService;

    public function __construct(PartnersService $partnersService)
    {
        $this->partnersService = $partnersService;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $query = $this->partnersService->hierarcyquery();

        return $people = Institute::from('institute_path')
            ->withRecursiveExpression('institute_path', $query)
            ->select('institute_path.path as institute', 'people.id', 'people.title', 'people.lastname', 'people.firstname', 'people.position', 'people.manager_id', 'people.description', 'people.created_at', 'people.updated_at',
                DB::raw("group_concat(DISTINCT emails.email_address separator '; ') as email_address, group_concat(DISTINCT phones.phone_number separator '; ') as phone_number" ))
            ->rightjoin('people', 'institute_path.id', '=', 'people.institute_id',)
            ->rightjoin('emails', 'people.id', '=', 'emails.person_id')
            ->rightjoin('phones', 'people.id', '=', 'phones.person_id')
            ->groupBy('people.id', 'people.title', 'people.lastname', 'people.firstname', 'people.position', 'people.manager_id', 'people.description', 'people.created_at', 'people.updated_at', 'institute')
            ->orderBy('people.lastname')
            ->orderBy('people.firstname')
            ->get();
    }

    /**
     * @param mixed $people
     * @return array
     */
    public function map($people): array
    {
        if($people->manager_id != null)
        {
            $people->manager_id = 'igen';
        } else
        {
            $people->manager_id = 'nem';
        }

        return [
            $people->id,
            $people->title,
            $people->lastname,
            $people->firstname,
            $people->position,
            $people->description,
            $people->manager_id,
            $people->institute,
            $people->email_address,
            $people->phone_number,
            $people->created_at,
            $people->updated_at
        ];
    }

    public function headings(): array
    {
        return ['ID', 'Titulus','Vezetéknév', 'Keresztnév', 'Beosztás', 'Megjegyzés', 'Vezető', 'Intézmény', 'E-mail', 'Telefon', 'Létrehozva', 'Módosítva'];
    }
}
