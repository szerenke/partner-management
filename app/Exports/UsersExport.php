<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UsersExport implements FromCollection, ShouldAutoSize, WithMapping, WithHeadings
{

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::with('roles')->get();
    }

    /**
     * @param mixed $users
     * @return array
     */
    public function map($users): array
    {
        return [
            $users->id,
            $users->lastname,
            $users->firstname,
            $users->email,
            $users->roles->first()->name,
            $users->created_at,
            $users->updated_at
        ];
    }

    public function headings(): array
    {
        return ['ID','Vezetéknév', 'Keresztnév', 'E-mail', 'Jogosultság', 'Létrehozva', 'Módosítva'];
    }
}
