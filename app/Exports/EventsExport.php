<?php

namespace App\Exports;

use App\Models\Event;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class EventsExport implements FromCollection, ShouldAutoSize, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Event::all();
    }

    public function map($events): array
    {
        return [
            $events->id,
            $events->event_name,
            $events->event_description,
            $events->event_location,
            $events->start_date,
            $events->end_date
        ];
    }

    public function headings(): array
    {
        return ['ID','Név', 'Leírás', 'Helyszín', 'Kezdés', 'Befejezés'];
    }
}
