<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'institute_name',
        'short_name',
        'category_id',
        'address_id',
        'parent_id'
    ];

    public function childinstitutes(){
        return $this->hasMany('App\Models\Institute', 'parent_id');
    }

    public function parentinstitute(){
        return $this->belongsTo('App\Models\Institute', 'parent_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function people()
    {
        return $this->hasMany(Person::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function specialities()
    {
        return $this->belongsToMany(Speciality::class);
    }
}
