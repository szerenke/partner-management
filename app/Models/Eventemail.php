<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Eventemail extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'sender',
        'email_subject',
        'email_message',
        'signature',
        'event_link',
        'sent_date',
        'event_id'
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
