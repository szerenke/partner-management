<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'lastname',
        'firstname',
        'title',
        'position',
        'gender',
        'manager_id',
        'description',
        'institute_id'
    ];

    public function emails()
    {
        return $this->hasMany(Email::class);
    }

    public function phones()
    {
        return $this->hasMany(Phone::class);
    }

    public function institute()
    {
        return $this->belongsTo(Institute::class);
    }

    public function events()
    {
        return $this->belongsToMany(Event::class, 'participants')->using(Participant::class);
    }

    public function presentations()
    {
        return $this->hasMany(Presentation::class);
    }
}
