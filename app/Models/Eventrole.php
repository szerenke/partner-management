<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Eventrole extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'eventrole_name',
        'rolecolour_class',
    ];

    public function participants()
    {
        return $this->belongsToMany(Participant::class, 'eventrole_participant', 'participant_id', 'eventrole_id');
    }
}
