<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'event_name',
        'event_description',
        'event_location',
        'start_date',
        'end_date',
    ];

    public function people()
    {
        return $this->belongsToMany(Person::class, 'participants')->using(Participant::class);
    }

    public function eventemails()
    {
        return $this->hasMany(Eventemail::class);
    }

    public function participants()
    {
        return$this->hasMany(Participant::class);
    }

    public function presentations()
    {
        return $this->hasMany(Presentation::class);
    }
}
