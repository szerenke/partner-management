<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Presentation extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'presentation_title',
        'presentation_description',
        'event_id',
        'person_id',
        'speciality_id'
    ];

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function event()
    {
        return $this->belongsTo(Person::class);
    }

    public function speciality()
    {
        return $this->belongsTo(Speciality::class);
    }
}
