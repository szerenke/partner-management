<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'postal_code',
        'address',
        'city_id'
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function institutes()
    {
        return $this->hasMany(Institute::class);
    }
}
