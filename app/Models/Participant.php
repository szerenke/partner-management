<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Participant extends Pivot
{
    use HasFactory;

    protected $table = 'participants';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'person_id',
        'event_id'
    ];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */

    //If the pivot model has a primary key it should be set true
    public $incrementing = true;

    public function eventroles()
    {
        return $this->belongsToMany(Eventrole::class, 'eventrole_participant', 'participant_id', 'eventrole_id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

}
