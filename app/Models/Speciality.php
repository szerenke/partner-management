<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'speciality_name',
        'colour_class',
    ];

    public function institutes()
    {
        return $this->belongsToMany(Institute::class);
    }

    public function presentations()
    {
        return $this->hasMany(Presentation::class);
    }
}
