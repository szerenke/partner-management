<?php

namespace App\Listeners;

use App\Mail\NewUserInformationMail;
use Illuminate\Support\Facades\Mail;

class InformNewUserListener
{

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        Mail::to($event->user->email)->send(new NewUserInformationMail($event->user));
    }
}
