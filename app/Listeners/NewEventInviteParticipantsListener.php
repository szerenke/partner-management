<?php

namespace App\Listeners;

use App\Mail\EventMessages;
use App\Models\Eventemail;
use App\Models\Person;
use Illuminate\Support\Facades\Mail;

class NewEventInviteParticipantsListener
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        foreach ($event->people_id as $person_id) {

            $eventemail = Eventemail::findOrfail($event->eventemail_id);

            $person = Person::findOrFail($person_id);
            $gender = $person->gender == 'male' ? 'Úr' : 'Asszony';
            $salutation = $person->manager_id != null ? mb_convert_case($person->position, MB_CASE_TITLE, 'UTF-8') : $person->lastname . ' ' . $person->firstname;
            $emails = $person->emails()->get();

            foreach ($emails as $email){

                Mail::to($email->email_address)->send(new EventMessages($gender, $salutation, $event->invitation_event, $eventemail));
            }
        }
    }
}
