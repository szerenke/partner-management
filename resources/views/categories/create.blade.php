@extends('layouts.app')

@section('content')
    <div class="page">
        @include('people.sidebar-people')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Új intézmény kategória') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <div class="card-header">

                            </div>

                            <div class="card-body">

                                <form method="POST" action="{{ route('categories.store') }}" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-group row">
                                        <label for="category_name" class="col-md-4 col-form-label text-md-right">{{ __('Kategória neve:') }}</label>

                                        <div class="col-md-6">
                                            <input type="text" class="form-control rounded @error('category_name') is-invalid @enderror" name="category_name" value="{{ old('category_name') }}"autocomplete="category_name">

                                            @error('category_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="card-footer">
                                        <div class="form-group row mb-0 justify-content-center">

                                            <div class="col-md-4 offset-md-0">
                                                <a href="{{ route('categories.index') }}"><button type="button" class="btn btn-secondary">@include('components.icons.left-icon'){{ __('Vissza') }}</button></a>
                                            </div>

                                            <div class="col-md-0 offset-md-4">
                                                <button type="submit" class="btn btn-primary">{{ __('Mentés') }}</button>
                                            </div>

                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
