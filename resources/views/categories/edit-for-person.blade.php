@extends('layouts.app')

@section('content')
    <div class="page">
        @include('people.sidebar-people')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Partner szerkesztése') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <div class="card-header text-center">
                                <h3 class="text-center">{{ $person->title }} {{ $person->lastname }} {{ $person->firstname }}</h3>
                            </div>

                            <form method="POST" action="{{ route('institutes.for.person', $person->id) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    @include('components.forms.select-category-form')
                                </div>
                                    @include('components.forms.partner-steps-footer', ['url' => 'people/'. $person->id])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
