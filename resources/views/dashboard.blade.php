@extends('layouts.app')

@section('content')
    <div class="sidebar d-flex min-vh-100 flex-column flex-shrink-0 p-3"></div>
    <div class="content pl-1 pr-1">
        <div class="d-flex welcome pt-4 pb-4 d-flex justify-content-center">{{ __('Partner nyilvántartó') }}</div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">

                    <div class="card">
                        <div class="card-header text-center">
                            <h5>{{ __('Sikeres bejelentkezés mint ') }} {{ Auth::user()->roles->first()->name }}</h5>
                        </div>
                        <div class="card-body pl-5 pr-5">
                            <h6>{{ __('Közelgő események: ') }}</h6>
                            @foreach($events as $event)
                                <div class="text-center"><strong class="pr-3">{{ $event->event_name }}</strong>{{ __('kezdés: ') }}  {{ date('Y.m.d H:i', strtotime($event->start_date)) }}</div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
@endsection
