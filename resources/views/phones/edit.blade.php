@extends('layouts.app')

@section('content')
    <div class="page">
        @include('people.sidebar-people')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Partner szerkesztése') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <div class="card-header text-center">
                                <div><h5>{{ $person->title }} {{ $person->lastname }} {{ $person->firstname }}</h5></div>
                                <div><h6>{{ __('Telefonszámok szerkesztése') }}</h6></div>
                            </div>

                            <div class="card-body">
                                @include('components.forms.person-phone-form')
                            </div>
                                @include('components.forms.back-footer', ['url' => 'people/'.$person->id])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
