@extends('layouts.app')

@section('content')
    <div class="page">
        @include('events.sidebar-events')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Résztvevő szerkesztése') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <div class="card-header">
                                <h5 class="text-center">{{ $event->event_name }}</h5>
                                <h3 class="text-center">{{ $person->title }} {{ $person->lastname }} {{ $person->firstname }}</h3>
                            </div>
                                <div class="card-body">

                                    <form method="POST" action="{{ route('participant.update.roles') }}" enctype="multipart/form-data">
                                    @csrf

                                    <!--Check input eventroles -->
                                        <label class="col-md-5 col-form-label text-md-right">{{ __('Részvétel kezelése:') }}</label>
                                        <div class="d-flex form-group row justify-content-center">
                                            <div class="form-check">
                                                <div class="form-check form-check-inline">
                                                    @foreach($eventroles as $eventrole)
                                                        @if($participant_roles->contains($eventrole->id) && $eventrole->eventrole_name === 'Meghívott')
                                                            <input onclick="return false;" name="eventroles[]" class="form-check-input" type="checkbox" value="{{ $eventrole->id }}" id="eventrole{{ $eventrole->id }}" checked>
                                                        @elseif($participant_roles->contains($eventrole->id) && $eventrole->eventrole_name != 'Meghívott')
                                                            <input name="eventroles[]" class="form-check-input" type="checkbox" value="{{ $eventrole->id }}" id="eventrole{{ $eventrole->id }}" checked>
                                                        @else
                                                            <input name="eventroles[]" class="form-check-input" type="checkbox" value="{{ $eventrole->id }}" id="eventrole{{ $eventrole->id }}">
                                                        @endif
                                                        <label class="form-check-label" for="eventrole{{ $eventrole->id }}">
                                                            <span class="badge badge-pill {{ $eventrole->rolecolour_class }}">{{ $eventrole->eventrole_name }}</span>
                                                            <span class="text-light">{{ __('---') }}</span>
                                                        </label>
                                                    @endforeach
                                                </div>
                                            </div>

                                            <div class="pl-4">
                                                <button type="submit" class="btn btn-primary">@include('components.icons.save-icon')</button>
                                            </div>

                                        </div>
                                    </form>

                                        @if(!is_null($presentation))
                                            <hr class="border-info">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">{{ __('Előadás címe:') }}</label>
                                                <label class="col-md-6 col-form-label text-md-left">{{ $presentation->presentation_title }}</label>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">{{ __('Előadás leírása:') }}</label>
                                                <label class="col-md-6 col-form-label text-md-left">{{ $presentation->presentation_description }}</label>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">{{ __('Szakterület:') }}</label>
                                                <label class="col-md-6 col-form-label text-md-left">{{ $speciality }}</label>
                                                <div>
                                                    <button class="btn btn-primary">
                                                        <a class="text-decoration-none text-light" href="{{ route('presentations.edit', $presentation->id) }}">@include('components.icons.edit-icon')</a>
                                                    </button>
                                                </div>

                                                <div class="pl-2">
                                                    <!-- Button trigger modal -->
                                                    <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $id = $presentation->id }}">
                                                        @include('components.icons.delete-icon')
                                                    </button>

                                                    <!-- Modal -->
                                                    @include('components.delede-modal', ['route' => 'presentations.destroy', 'headertext' => 'Előadás törlése',
                                                              'bodytext' => 'Biztosan törölni akarja az előadást minden adatával?' ])

                                                </div>

                                            </div>
                                        @elseif(is_null($presentation) && $participant_roles->contains($presenterRole->id))
                                            <hr class="border-info">
                                            <div class="form-group row justify-content-center">
                                                <button class="btn btn-primary">
                                                    <a class="text-decoration-none text-light" href="{{ route('presentations.create') }}">{{ __('Előadás hozzáadása') }}</a>
                                                </button>
                                            </div>
                                        @endif
                                </div>
                                    @include('components.forms.back-footer', ['url' => 'events/'.$event->id])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
