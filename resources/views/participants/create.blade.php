@extends('layouts.app')

@section('content')
    <div class="page">
        @include('events.sidebar-events')

        <div class="content pl-1 pr-1">
            <div Class=" welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ $event->event_name }}</h1></div>
            <div class="container user-table">

                <form method="POST" action="{{ route('participants.store') }}" enctype="multipart/form-data">

                    @livewire('select-new-participants', ['event' => $event])

                    @include('components.forms.edit-footer')

                </form>

            </div>
        </div>
    </div>

@endsection
