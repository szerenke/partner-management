@component('mail::message')
## Kedves {{ $salutation}} {{ $gender }}!

{{ $eventemail->email_message }}

## Esemény időpontja: {{ date('Y.m.d H:i', strtotime($invitation_event->start_date)) }} {{ __(' - ') }} {{ date('Y.m.d H:i', strtotime($invitation_event->end_date)) }}
## Helyszín: {{ $invitation_event->event_location}}

Üdvözlettel:

<p>{{ $eventemail->signature }}</p>

@if($eventemail->event_link != null)
@component('mail::button', ['url' => $eventemail->event_link])
    {{ $invitation_event->event_name}}
@endcomponent
@endif

<table>
<tr><td style="margin:0;padding:0;line-height:15px;"><a style="line-height:15px;color:#1166b6;text-decoration:none;" href="https://mik.pte.hu" target="_blank" >https://mik.pte.hu</a> | <a style="line-height:15px;color:#1166b6;text-decoration:none;" href="http://facebook.com/ptemik" target="_blank">http://facebook.com/ptemik</a></td></tr>
</table>
<hr>
<table style="margin:0;padding:0;font-family:'Arial','Helvetica',sans-serif;font-size:13px;line-height:15px;">
    <tr><td style="margin:0;padding:0;line-height:15px;color:#666;"><strong style="line-height:15px;">Pécsi Tudományegyetem &diams; University of Pécs</strong></td></tr>
    <tr><td style="margin:0;padding:0;line-height:20px;color:#666;">Magyarország első egyeteme <sup>&reg;</sup> - 1367 &diams; The first University of Hungary <sup>&reg;</sup> - 1367</td></tr>
</table>

@endcomponent
