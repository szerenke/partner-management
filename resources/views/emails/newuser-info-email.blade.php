@component('mail::message')
# Felhasználói fiók regisztrálása

Kedves {{ $user->lastname}} {{ $user->firstname}}!

Az Ön nevére felhasználói fiókot hoztunk létre a Partner nyilvántartó alkalmazáshoz az alábbi e-mail címen:

<div>
   <strong class="text-center">{{ $user->email}}</strong>
</div>

A belépéshez szükséges alapértelmezett jelszó a számsor első nyolc számjegye.

Kérjük, hogy mielőbb változtassa meg a jelszavát.

@component('mail::button', ['url' => 'http://127.0.0.1:8000'])
    Belépés
@endcomponent

<br>
<table style="margin:0;padding:0 0 20px 0;font-family:'Arial','Helvetica',sans-serif;font-size:12px;line-height:14px;">
    <tr><td style="margin:0;padding:0;font-size:12px;line-height:14px;color:#666;"><strong style="line-height:14px;">--<br>Üdvözlettel: &diams; Yours sincerely:</strong></td></tr>
</table>
<table style="margin:0;padding:0;font-family:'Arial','Helvetica',sans-serif;font-size:13px;line-height:15px;">
    <tr>
        <td style="margin:0;padding:0;"><img src="http://static.mik.pte.hu/vcard_logo.png" alt="logo"></td>
        <td style="margin:0;padding:0;">
            <table style="margin:0;padding:0; font-family:'Arial','Helvetica',sans-serif;font-size:13px;line-height:15px;color:black;">
                <tr><td style="margin:0;padding:0;font-size:16px;line-height:18px;"><strong style="line-height:18px;">Zseni Andrea</strong></td></tr>
                <tr><td style="margin:0;padding:0 0 4px 0;line-height:15px;border-bottom:2px solid black;">ügyvivő szakértő</td></tr>
                <tr><td style="margin:0;padding:4px 0 0 0;line-height:15px;"><strong style="line-height:15px;">Pécsi Tudományegyetem</strong></td></tr>
                <tr><td style="margin:0;padding:0;line-height:15px;">Műszaki és Informatikai Kar</td></tr>
                <tr><td style="margin:0;padding:0 0 23px 0;line-height:15px;">Dékáni Hivatal - Dékáni Titkárság</td></tr>
                <tr><td style="margin:0;padding:0;line-height:15px;">H-7624 Pécs | Boszorkány út 2.</td></tr>
                <tr><td style="margin:0;padding:0;line-height:15px;"><a style="line-height:15px;color:#000;text-decoration:none;" href="tel:+36 72 503 650/22808">T: +36 72 503 650/22808</a></td></tr>
                <tr><td style="margin:0;padding:0;line-height:15px;">E.: <a style="line-height:15px;color:#000000;text-decoration:none;" href="mailto:zseni.andrea@mik.pte.hu" target="_blank">zseni.andrea@mik.pte.hu</a></td></tr>
            </table>
        </td>
    </tr>
</table>

<table style="margin:0;padding:20px 0 0 0;font-family:'Arial','Helvetica',sans-serif;font-size:13px;line-height:15px;">
    <tr><td style="margin:0;padding:0;line-height:15px;"><a style="line-height:15px;color:#1166b6;text-decoration:none;" href="https://mik.pte.hu" target="_blank" >https://mik.pte.hu</a> | <a style="line-height:15px;color:#1166b6;text-decoration:none;" href="http://facebook.com/ptemik" target="_blank">http://facebook.com/ptemik</a></td></tr>
</table>
<hr>
<table style="margin:0;padding:0;font-family:'Arial','Helvetica',sans-serif;font-size:13px;line-height:15px;">
    <tr><td style="margin:0;padding:0;line-height:15px;color:#666;"><strong style="line-height:15px;">Pécsi Tudományegyetem &diams; University of Pécs</strong></td></tr>
    <tr><td style="margin:0;padding:0;line-height:20px;color:#666;">Magyarország első egyeteme <sup>&reg;</sup> - 1367 &diams; The first University of Hungary <sup>&reg;</sup> - 1367</td></tr>
</table>
@endcomponent
