@extends('layouts.app')

@section('content')
    <div class="page">
        @include('events.sidebar-events')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Új résztvevők') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <div class="card-header text-center">
                                <h5>{{ __('Email kiválasztása:') }}</h5>
                            </div>

                            <div class="card-body">

                                <form method="POST" action="{{ route('participants.eventemail.selected') }}" enctype="multipart/form-data">
                                @csrf
                                <!--Select Event Email -->
                                    <div class="form-group row">
                                        <label for="mail_id" class="col-md-4 col-form-label text-md-right">{{ __('Esemény email:') }}</label>

                                        <div class="col-md-6">
                                            @if(count($eventemails) != 0)
                                                <select name="mail_id"  class="form-select form-select-sm btn-sm">
                                                    @foreach($eventemails as $mail)
                                                        <option value="{{ $mail->id }}" >{{ $mail->email_subject }}</option>
                                                    @endforeach
                                                </select>
                                            @else
                                                <label class="col-form-label text-md-right">{{ __('Még nincsenek kiválasztható e-mailek') }}</label>
                                            @endif
                                        </div>
                                    </div>
                                    @if(count($eventemails) != 0)
                                        @include('components.forms.edit-footer')
                                    @else
                                        @include('components.forms.back-footer', ['url' => '/events/'.$event_id])
                                    @endif

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
