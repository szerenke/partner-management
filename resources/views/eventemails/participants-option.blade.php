@extends('layouts.app')

@section('content')
    <div class="page">
        @include('events.sidebar-events')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Új résztvevők') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <div class="card-header text-center">
                                <h5>{{ __('Hozzáadás módja:') }}</h5>
                            </div>

                            <div class="card-body">

                                <form method="POST" action="{{ route('participants.select.event.email') }}" enctype="multipart/form-data">
                                @csrf

                                <!--Select Option new participants -->
                                    <div class="form-group row pl-lg-5">
                                        <div class="col-md-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="participants_option" id="option_1" value="1" checked>
                                                <label class="form-check-label" for="participants_option">{{ __('Hozzáadás meghívó küldésével') }}</label>
                                                <br>
                                                <input class="form-check-input" type="radio" name="participants_option" id="option_0" value="0">
                                                <label class="form-check-label" for="participants_option">{{ __('Hozzáadás meghívó nélkül visszaigazolt új résztvevőknek') }}</label>
                                            </div>
                                        </div>
                                    </div>

                                    @include('components.forms.partner-steps-footer', ['url' => '/events/'. $event_id])

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
