@extends('layouts.app')

@section('content')
    <div class="page">
        @include('events.sidebar-events')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Esemény e-mailek') }}</h1></div>
            <div class="container user-table col-md-12">

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th colspan="8" class="text-center">{{ $event->event_name }}</th>
                        </tr>
                        <tr>
                            <th scope="col-sm-1">{{ __('Sorszám') }}</th>
                            <th scope="col">{{ __('Küldő') }}</th>
                            <th scope="col">{{ __('Tárgy') }}</th>
                            <th scope="col">{{ __('Üzenet') }}</th>
                            <th scope="col">{{ __('Aláírás') }}</th>
                            <th scope="col">{{ __('Link') }}</th>
                            @if(Auth::user()->hasRole('admin'))
                                <th class="small-td">{{ __('Szerkeszt') }}</th>
                                <th class="small-td">{{ __('Töröl') }}</th>
                            @endif
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($eventemails as $email)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $email->sender }}</td>
                            <th scope="row">{{ $email->email_subject }}</th>
                            <td>{{ $email->email_message }}</td>
                            <td>{{ $email->signature }}</td>
                            <td>{{ $email->event_link }}</td>
                            @if(Auth::user()->hasRole('admin'))
                                @if($event->start_date > now())
                                <td>
                                    <button class="btn btn-primary">
                                        <a class="text-decoration-none text-light" href="{{ route('eventemails.edit',$email->id)}}" >
                                            @include('components.icons.edit-icon')
                                        </a>
                                    </button>
                                </td>

                                <td>
                                @if($email->sent_date === null)
                                <!-- Button trigger modal -->
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $id = $email->id }}">
                                        @include('components.icons.delete-icon')
                                    </button>

                                    <!-- Modal -->
                                    @include('components.delede-modal', ['route' => 'eventemails.destroy', 'headertext' => 'Esemény e-mail törlése',
                                            'bodytext' => 'Biztosan törölni akarja az esemény e-mailt minden adatával?' ])

                                @endif
                                </td>
                                @endif
                            @endif
                        </tr>
                        @endforeach
                        @if(Auth::user()->hasRole('admin'))
                        <tr>
                            <td colspan="8" class="text-center">
                                <button class="btn btn-primary">
                                    <a class="text-decoration-none text-light" href="{{ route('eventemails.create') }}">{{ __('Esemény e-mail hozzáadása') }}</a>
                                </button>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                @include('components.forms.back-footer', ['url' => 'events/'.$event->id])
            </div>
            @if(session('status'))
                <div class="alert alert-danger">
                    {{ session('status') }}
                </div>
            @endif
        </div>
    </div>

@endsection
