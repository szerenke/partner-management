<nav class="navbar navbar-expand-lg navbar-light top-nav bg-white border-b border-gray-100 fixed-top">
    <div class="container">
        <a class="navbar-brand d-flex" href="{{ url('/dashboard') }}"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <img src="/picture/mik_logo2.png" alt="MIK_logo" width="50" height="50">
                    </li>
                    <li class="nav-item pl-5">
                        <a class="nav-link text-dark" aria-current="page" href="{{ route('dashboard') }}">@include('components.icons.home-icon')</a>
                    </li>

                    @if(Auth::user()->hasRole('admin'))
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="{{ route('users.index') }}">{{ __('Felhasználók') }}</a>
                    </li>
                    @endif

                    <li class="nav-item">
                        <a class="nav-link text-dark" href="{{ route('people.index') }}">{{ __('Partnerek') }}</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link text-dark" href="{{ route('events.index') }}">{{ __('Események') }}</a>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle text-dark" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->lastname }} {{ Auth::user()->firstname }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item text-dark" href="{{ route('dashboard.myprofile') }}">
                                @include('components.icons.user-icon')
                                {{ __('Profil') }}
                            </a>
                            <a class="dropdown-item text-dark" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                @include('components.icons.logout-icon')
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>

            </div>
    </div>
</nav>
