@extends('layouts.app')

@section('content')
    <div class="page">
        <div class="sidebar d-flex min-vh-100 flex-column flex-shrink-0"></div>
        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Profilom') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="text-center">{{Auth::user()->lastname}} {{Auth::user()->firstname}}</h3>
                            </div>

                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label text-md-right">{{ __('Jogosultság:') }}</label>
                                    @if(Auth::user()->hasRole('admin'))
                                        <label class="col-md-5 col-form-label text-md-left">{{ __('adminisztrátor') }}</label>
                                    @else
                                        <label class="col-md-5 col-form-label text-md-left">{{ __('felhasználó') }}</label>
                                    @endif

                                    <div class="col-md-3">
                                        <button class="btn btn-primary">
                                            <a class="text-light" href="{{ route('profile.edit',Auth::user()-> id )}}">@include('components.icons.edit-icon')</a>
                                        </button>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label text-md-right">{{ __('E-mail:') }}</label>
                                    <label class="col-md-5 col-form-label text-md-left">{{Auth::user()->email}}</label>

                                </div>

                            </div>
                            <div class="card-footer">
                                <div class="form-group row mb-0 justify-content-center">

                                    <div class="col-md-4 offset-md-0">
                                        <button type="button" class="btn btn-secondary">
                                            <a class="text-decoration-none text-light" href="{{ route('dashboard') }}">@include('components.icons.left-icon'){{ __('Vissza') }}</a>
                                        </button>
                                    </div>

                                    <div class="col-md-0 offset-md-4">
                                        <button class="btn btn-primary"><a class="text-light" href="{{ route('profile.edit-password',Auth::user()-> id)}}">{{ __('Jelszó módosítás') }}</a></button>
                                    </div>

                                </div>
                            </div>

                            @if(session('status'))
                                <div class="alert alert-danger">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
