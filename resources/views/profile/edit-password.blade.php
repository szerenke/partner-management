@extends('layouts.app')

@section('content')
    <div class="page">
        <div class="sidebar d-flex min-vh-100 flex-column flex-shrink-0"></div>

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Jelszó módosítás') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">

                        <div class="card">
                            <div class="card-header text-center">
                                <h6>{{ $user->lastname }} {{ $user->firstname }}</h6>
                            </div>

                            <form method="POST" action="/profile/{{ $user->id }}/update-password"  enctype="multipart/form-data">
                                @csrf
                            <div class="card-body">

                                <div class="form-group row">
                                    <label for="current_password" class="col-md-4 col-form-label text-md-right">{{ __('Régi jelszó') }}</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control rounded @error('current_password') is-invalid @enderror" name="current_password" autocomplete="current-password">

                                        @error('current_password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="new_password" class="col-md-4 col-form-label text-md-right">{{ __('Új jelszó') }}</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control rounded @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                                        <label class="text-secondary info-font" for="password">@include('components.icons.info-icon'){{ __(' Min. 8 karakter, legalább egy-egy kis-és nagybetűt, egy számot és egy speciális karaktert kell tartalmaznia. ') }}</label>

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="confirm_password" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control rounded" name="confirm_password" autocomplete="new-password">
                                    </div>
                                </div>
                            </div>
                                @include('components.forms.edit-footer')

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
