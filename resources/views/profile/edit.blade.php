@extends('layouts.app')

@section('content')
    <div class="page">
        <div class="sidebar d-flex min-vh-100 flex-column flex-shrink-0"></div>

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Profil szerkesztése') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">

                        <div class="card">
                            <form method="POST" action="/profile/{{ $user->id }}"  enctype="multipart/form-data">
                                @csrf
                            <div class="card-body">
                                @include('components.forms.user-form')
                            </div>
                                @include('components.forms.edit-footer')
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
