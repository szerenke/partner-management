@extends('layouts.app')

@section('content')
    <div class="page">
        @include('events.sidebar-events')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Előadások') }}</h1></div>
            <div class="container user-table col-md-10">

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th colspan="5" class="text-center">{{ $event->event_name }}</th>
                        </tr>
                        <tr>
                            <th scope="col-sm-1">{{ __('Sorszám') }}</th>
                            <th scope="col">{{ __('Cím') }}</th>
                            <th scope="col">{{ __('Leírás') }}</th>
                            <th scope="col">{{ __('Előadó') }}</th>
                            <th scope="col">{{ __('Szakterület') }}</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($presentations as $presentation)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <th scope="row">{{ $presentation->presentation_title }}</th>
                            <td>{{ $presentation->presentation_description }}</td>
                            <td>
                                @isset($presentation->person_id)
                                    {{ $presentation->person->title }} {{ $presentation->person->lastname }} {{ $presentation->person->firstname }}
                                @endif
                            </td>
                            <td>
                                @isset($presentation->speciality_id)
                                    {{ $presentation->speciality->speciality_name }}
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @include('components.forms.back-footer', ['url' => 'events/'.$event->id])
            </div>
        </div>
    </div>

@endsection
