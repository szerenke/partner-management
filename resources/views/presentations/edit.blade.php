@extends('layouts.app')

@section('content')
    <div class="page">
        @include('events.sidebar-events')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Előadás szerkesztése') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <div class="card-header">
                                <h5 class="text-center">{{ $event->event_name }}</h5>
                                <h3 class="text-center">{{ $person->title }} {{ $person->lastname }} {{ $person->firstname }}</h3>
                            </div>


                            <form method="POST" action="{{ route('presentations.update', $presentation->id) }}" enctype="multipart/form-data">
                                @csrf
                            <div class="card-body">

                                <div class="form-group row">
                                    <label for="presentation_title" class="col-md-4 col-form-label text-md-right">{{ __('Előadás címe') }}<span class="text-danger">{{ __('* ') }}</span></label>

                                    <div class="col-md-6">
                                        <input id="presentation_title" type="text" class="form-control rounded @error('presentation_title') is-invalid @enderror" name="presentation_title" value="{{ $presentation->presentation_title ?? old('presentation_title') }}" autocomplete="presentation_title" autofocus>

                                        @error('presentation_title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="presentation_description" class="col-md-4 col-form-label text-md-right">{{ __('Előadás leírása') }}</label>

                                    <div class="col-md-6">
                                        <textarea id="presentation_description" type="text" class="form-control rounded" name="presentation_description" autocomplete="presentation_description">
                                            {{ $presentation->presentation_description ?? old('presentation_description') }}
                                        </textarea>

                                        @error('presentation_description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="presentation_description" class="col-md-4 col-form-label text-md-right">{{ __('Szakterület kiválasztása') }}</label>

                                </div>
                                <div class="form-group row justify-content-center pb-3">
                                    <div class="form-check d-flex justify-content-center">
                                        <div class="form-check form-check-inline ">
                                            @foreach($specialities as $speciality)
                                                @if($speciality->id === $speciality_id)
                                                    <div class="pl-3">
                                                        <input name="speciality" class="form-check-input" type="radio" value="{{ $speciality->id }}" {{ old('speciality_id', $speciality_id) === $speciality->id  ? 'checked' : '' }} id="speciality{{ $speciality->id }}">
                                                    </div>
                                                @else
                                                    <div class="pl-3">
                                                        <input name="speciality" class="form-check-input" type="radio" value="{{ $speciality->id }}" id="speciality{{ $speciality->id }}">
                                                    </div>
                                                @endif
                                                <span class="badge badge-pill {{ $speciality->colour_class }}">{{ $speciality->speciality_name }}</span>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @include('components.forms.edit-footer')

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
