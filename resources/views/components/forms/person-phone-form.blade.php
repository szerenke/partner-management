@foreach($person->phones as $phone)
    <div class="row">

        <div class="col-md-10">
            <form method="POST" action="{{ route('phones.update', $phone->id)}}" >
                @csrf
                <div class="form-group row justify-content-start">
                    <label for="phone" class="col-md-3 col-form-label text-md-right">{{ __('Telefon ') }} {{ ++$i }}</label>

                    <div class="col-md-7">
                        <input type="text" class="form-control rounded @error('phone_number'.$i) is-invalid @enderror" name="phone_number{{ $i }}" value="{{ $phone->phone_number  ?? old('phone_number'.$i) }}"autocomplete="phone_number{{ $i }}">

                        @error('phone_number'.$i)
                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                        @enderror
                    </div>
                    <div><button type="submit" class="btn btn-primary">@include('components.icons.save-icon')</button></div>
                    <input readonly hidden type="text" name="id" id="id" value="{{ $id = $phone->id }}">
                    <input readonly hidden type="text" name="index" id="index" value="{{ $index = $i }}">
                </div>
            </form>
        </div>

        <div class="col-md-2">
            <!-- Button trigger modal -->
            <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $id }}">
                @include('components.icons.delete-icon')
            </button>
            <!-- Modal -->
            @include('components.delede-modal', ['route' => 'phones.destroy', 'headertext' => 'E-mail cím törlése',
                    'bodytext' => 'Biztosan törölni akarja az e-mail címet?' ])

        </div>
    </div>
@endforeach

<div class="row">
    <div class="col-md-10">

        <form method="POST" action="{{ route('phones.store') }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group row justify-content-start">
                <label for="phone_number" class="col-md-3 col-form-label text-md-right">{{ __('Új telefon ') }}</label>

                <div class="col-md-7">
                    <input type="texr" class="form-control rounded @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}"autocomplete="phone_number">

                    @error('phone_number')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <input readonly hidden type="text" name="person_id" id="index" value="{{ $person->id }}">
                <div><button type="submit" class="btn btn-primary">@include('components.icons.save-icon')</button></div>
            </div>
        </form>
    </div>
</div>
