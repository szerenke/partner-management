<!--Select Option manager -->
<div class="form-group row">
    <label for="manager_id" class="col-md-4 col-form-label text-md-right">{{ __('Vezető:') }}</label>

    <div class="col-md-6">
        <select name="manager_id"  class="form-select form-select-sm btn-sm">
            <option value="0" {{ old('manager_id', $person->manager_id) == null ? 'selected' : '' }}>{{ __('Nem') }}</option>
            <option value="1" {{ old('manager_id', $person->manager_id) == $person->id ? 'selected' : '' }}>{{ __('Igen') }}</option>
        </select>
    </div>
</div>

<!--Select Option gender type-->
<div class="form-group row">
    <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Neme:') }}</label>

    <div class="col-md-6">
        <select name="gender" class="form-select form-select-sm btn-sm">
            <option value="male" {{ old('gender', $person->gender) == 'male' ? 'selected' : '' }}>{{ __('Férfi') }}</option>
            <option value="female" {{ old('gender', $person->gender) == 'female' ? 'selected' : '' }}>{{ __('Nő') }}</option>
        </select>
    </div>
</div>
