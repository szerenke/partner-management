<!--Select Option institute -->
<div class="form-group row">
    <label for="institute_id" class="col-md-3 col-form-label text-md-right">{{ __('Intézmény:') }}</label>

    <div class="col-md-6">
        <select name="institute_id"  class="form-control">
            @if($individual != null)
                <option value="{{ $individual->id }}">{{ $individual->institute_name }}</option>
            @else
                @foreach($institutes as $institute)
                    <option value="{{ $institute->id }}" {{ old('institute_id', $person->institute_id) ==  $institute->id  ? 'selected' : '' }}>{{ $institute->path }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
