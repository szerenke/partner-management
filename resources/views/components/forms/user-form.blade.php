<div class="form-group row">
    <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Vezetéknév') }}</label>

    <div class="col-md-6">
        <input id="lastname" type="text" class="form-control rounded @error('lastname') is-invalid @enderror" name="lastname" value="{{ $user->lastname ?? old('lastname') }}"autocomplete="lastname" autofocus>

        @error('lastname')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('Keresztnév') }}</label>

    <div class="col-md-6">
        <input id="firstname" type="text" class="form-control rounded" name="firstname" value="{{ $user->firstname ?? old('firstname') }}"autocomplete="firstname" autofocus>

    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail cím') }}</label>

    <div class="col-md-6">
        <input id="email" type="email" class="form-control rounded @error('email') is-invalid @enderror" name="email" value="{{ $user->email ?? old('email') }}"autocomplete="email">

        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>


