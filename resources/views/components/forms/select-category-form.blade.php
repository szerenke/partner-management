<!--Select Option institute category -->
<div class="form-group row">
    <label for="category_id" class="col-md-4 col-form-label text-md-right">{{ __('Intézmény kategória:') }}</label>

    <div class="col-md-4">
        <select name="category_id"  class="form-control">
            @foreach($categories as $category)
                <option value="{{ $category->id }}" {{ old('category_id', $category_id) ==  $category->id  ? 'selected' : '' }}>{{ $category->category_name }}</option>
            @endforeach
        </select>
    </div>
    @if(request()->session()->get('url') === '/categories')
        <a class="text-primary text-decoration-none" href="{{ route('categories.create') }}">@include('components.icons.plus-icon')</a>
    @endif
</div>
