<div class="form-group row">
    <label for="event_name" class="col-md-4 col-form-label text-md-right">{{ __('esemény neve') }}<span class="text-danger">{{ __('* ') }}</span></label>

    <div class="col-md-6">
        <input id="event_name" type="text" class="form-control rounded @error('event_name') is-invalid @enderror" name="event_name" value="{{ $event->event_name ?? old('event_name') }}" autocomplete="event_name" autofocus>

        @error('event_name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="event_description" class="col-md-4 col-form-label text-md-right">{{ __('Esemény leírása') }}</label>

    <div class="col-md-6">
        <textarea id="event_description" type="text" class="text-dark form-control rounded @error('event_description') is-invalid @enderror" name="event_description" autocomplete="event_description" autofocus>
            {{ $event->event_description ?? old('event_description') }}
        </textarea>

        @error('event_description')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="event_location" class="col-md-4 col-form-label text-md-right">{{ __('Esemény helyszíne') }}</label>

    <div class="col-md-6">
        <input id="event_location" type="text" class="form-control rounded @error('event_location') is-invalid @enderror" name="event_location" value="{{ $event->event_location ?? old('event_location') }}" autocomplete="event_location" autofocus>

        @error('event_location')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="start_date" class="col-md-4 col-form-label text-md-right">{{ __('Kezdés dátuma') }}<span class="text-danger">{{ __('* ') }}</span></label>

    <div class="col-md-6">
        @isset($event)
            <input id="start_date" type="datetime-local" class="form-control rounded @error('start_date') is-invalid @enderror" name="start_date" value="{{ date("Y-m-d\TH:i:s", strtotime($event->start_date)) ?? old('start_date') }}" autocomplete="start_date" autofocus>
        @else
            <input id="start_date" type="datetime-local" class="form-control rounded @error('start_date') is-invalid @enderror" name="start_date" value="{{ old('start_date') }}" autocomplete="start_date" autofocus>
        @endif
        @error('start_date')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="end_date" class="col-md-4 col-form-label text-md-right">{{ __('Befejezés dátuma') }}<span class="text-danger">{{ __('* ') }}</span></label>

    <div class="col-md-6">
        @isset($event)
            <input id="end_date" type="datetime-local" class="form-control rounded @error('end_date') is-invalid @enderror" name="end_date" value="{{ date("Y-m-d\TH:i:s", strtotime($event->end_date)) ?? old('start_date') ?? old('end_date') }}" autocomplete="end_date" autofocus>
        @else
            <input id="end_date" type="datetime-local" class="form-control rounded @error('end_date') is-invalid @enderror" name="end_date" value="{{ old('end_date') }}" autocomplete="end_date" autofocus>
        @endif
        @error('end_date')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>


