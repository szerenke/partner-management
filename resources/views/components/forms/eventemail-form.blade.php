<div class="form-group row">
    <label for="sender" class="col-md-3 col-form-label text-md-right">{{ __('Küldő') }}<span class="text-danger">{{ __('* ') }}</span></label>

    <div class="col-md-7">
        <input id="sender" type="email" class="form-control rounded @error('sender') is-invalid @enderror" name="sender" value="{{ $eventemail->sender ?? old('sender') }}" autocomplete="sender" autofocus>

        @error('sender')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="email_subject" class="col-md-3 col-form-label text-md-right">{{ __('Tárgy') }}<span class="text-danger">{{ __('* ') }}</span></label>

    <div class="col-md-7">
        <input id="email_subject" type="text" class="form-control rounded @error('email_subject') is-invalid @enderror" name="email_subject" value="{{ $eventemail->email_subject ?? old('email_subject') }}" autocomplete="email_subject" autofocus>

        @error('email_subject')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="email_message" class="col-md-3 col-form-label text-md-right">{{ __('Üzenet') }}<span class="text-danger">{{ __('* ') }}</span></label>

    <div class="col-md-7">
        <textarea id="email_message" type="text" class="form-control rounded" name="email_message" autocomplete="email_message">
            {{ $eventemail->email_message ?? old('email_message') }}
        </textarea>

        @error('email_message')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="signature" class="col-md-3 col-form-label text-md-right">{{ __('Aláírás') }}<span class="text-danger">{{ __('* ') }}</span></label>

    <div class="col-md-7">
        <textarea id="signature" type="text" class="form-control rounded" name="signature" autocomplete="signature">
            {{ $eventemail->signature ?? old('signature') }}
        </textarea>

        @error('signature')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="event_link" class="col-md-3 col-form-label text-md-right">{{ __('Link') }}</label>

    <div class="col-md-7">
        <input id="event_link" type="text" class="form-control rounded @error('event_link') is-invalid @enderror" name="event_link" value="{{ $eventemail->event_link ?? old('signature') }}" autocomplete="event_link">

        @error('event_link')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
