<!--Form footer for all steps to create partners and institutes -->
<div class="card-footer">
    <div class="form-group row mb-0 justify-content-center">

        <div class="col-md-4 offset-md-0">
            <button type="button" class="btn btn-secondary">
                <a class="text-decoration-none text-light" href="{{ url($url) }}">@include('components.icons.left-icon'){{ __('Vissza') }}</a>
            </button>
        </div>

        <div class="col-md-0 offset-md-4">
            <button type="submit" class="btn btn-primary">{{ __('Mentés') }}@include('components.icons.right-icon')</button>
        </div>

    </div>
</div>
