@foreach($person->emails as $email)
    <div class="row">

        <div class="col-md-10">
            <form method="POST" action="{{ route('mails.update', $email->id)}}">
                @csrf
                <div class="form-group row justify-content-start">
                    <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('E-mail ') }} {{ ++$i }}</label>

                    <div class="col-md-7">
                        <input type="email" class="form-control rounded @error('email_address'.$i) is-invalid @enderror" name="email_address{{ $i }}" value="{{ $email->email_address  ?? old('email_address'.$i) }}"autocomplete="email_address{{ $i }}">

                        @error('email_address'.$i)
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div><button type="submit" class="btn btn-primary">@include('components.icons.save-icon')</button></div>
                    <input readonly hidden type="text" name="id" id="id" value="{{ $id = $email->id }}">
                    <input readonly hidden type="text" name="index" id="index" value="{{ $index = $i }}">
                </div>
            </form>
        </div>

        <div class="col-md-2">
            <!-- Button trigger modal -->
            <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $id }}">
                @include('components.icons.delete-icon')
            </button>
            <!-- Modal -->
            @include('components.delede-modal', ['route' => 'mails.destroy', 'headertext' => 'E-mail cím törlése',
                    'bodytext' => 'Biztosan törölni akarja az e-mail címet?' ])

        </div>
    </div>
@endforeach

<div class="row">
    <div class="col-md-10">

        <form method="POST" action="{{ route('mails.store') }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group row justify-content-start">
                <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('Új E-mail ') }}</label>

                <div class="col-md-7">
                    <input type="email" class="form-control rounded @error('email_address') is-invalid @enderror" name="email_address" value="{{ old('email_address') }}"autocomplete="email_address">

                    @error('email_address')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <input readonly hidden type="text" name="person_id" id="index" value="{{ $person->id }}">
                <div><button type="submit" class="btn btn-primary">@include('components.icons.save-icon')</button></div>
            </div>
        </form>
    </div>
</div>
