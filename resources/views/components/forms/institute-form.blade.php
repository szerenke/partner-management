<div class="form-group row">
    <label for="institute_name" class="col-md-4 col-form-label text-md-right">{{ __('Intézmény neve') }}<span class="text-danger">{{ __('* ') }}</span></label>

    <div class="col-md-6">
        <input id="institute_name" type="text" class="form-control rounded @error('institute_name') is-invalid @enderror" name="institute_name" value="{{ $institute->institute_name ?? old('institute_name') }}" autocomplete="institute_name" autofocus>

        @error('institute_name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="short_name" class="col-md-4 col-form-label text-md-right">{{ __('Rövid név') }}</label>

    <div class="col-md-6">
        <input id="short_name" type="text" class="form-control rounded @error('short_name') is-invalid @enderror" name="short_name" value="{{ $institute->short_name ?? old('short_name') }}" autocomplete="short_name" autofocus>

        @error('short_name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
