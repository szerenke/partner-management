<div class="form-group row">
    <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Titulus') }}</label>

    <div class="col-md-6">
        <input id="title" type="text" class="form-control rounded @error('title') is-invalid @enderror" name="title" value="{{ $person->title ?? old('title') }}" autocomplete="title" autofocus>

        @error('title')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Vezetéknév') }}<span class="text-danger">{{ __('* ') }}</span></label>

    <div class="col-md-6">
        <input id="lastname" type="text" class="form-control rounded @error('lastname') is-invalid @enderror" name="lastname" value="{{ $person->lastname ?? old('lastname') }}" autocomplete="lastname" autofocus>

        @error('lastname')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('Keresztnév') }}<span class="text-danger">{{ __('* ') }}</span></label>

    <div class="col-md-6">
        <input id="firstname" type="text" class="form-control rounded @error('firstname') is-invalid @enderror" name="firstname" value="{{ $person->firstname ?? old('firstname') }}" autocomplete="firstname" autofocus>

        @error('firstname')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="position" class="col-md-4 col-form-label text-md-right">{{ __('Beosztás') }}</label>

    <div class="col-md-6">
        <input id="position" type="text" class="form-control rounded @error('position') is-invalid @enderror" name="position" value="{{ $person->position ?? old('position') }}" autocomplete="position">

        @error('position')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Leírás') }}</label>

    <div class="col-md-6">
        <textarea id="description" type="text" class="form-control rounded @error('description') is-invalid @enderror" name="description" autocomplete="description">
            {{ $person->description ?? old('description') }}
        </textarea>

        @error('description')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

    </div>
</div>
