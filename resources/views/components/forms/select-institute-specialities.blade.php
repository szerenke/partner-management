<!--Check input specialities -->
<label class="col-md-3 col-form-label text-md-right">{{ __('Szakterület:') }}</label>
<div class="form-group row justify-content-center">
    <div class="form-check">
        <div class="form-check form-check-inline">
            @foreach($specialities as $speciality)
                @foreach($institute_specialities as $institute_speciality)
                    @if($speciality->id === $institute_speciality->id)
                        <div class="pl-3">
                            <input name="specialities[]" class="form-check-input" type="checkbox" value="{{ $speciality->id }}" {{ old('speciality_id', $institute_speciality->id) ==  $speciality->id  ? 'checked' : '' }} id="speciality{{ $speciality->id }}">
                        </div>
                    @endif
                @endforeach
                @if(!$institute_specialities->contains($speciality->id))
                    <div class="pl-3">
                        <input name="specialities[]" class="form-check-input" type="checkbox" value="{{ $speciality->id }}" id="speciality{{ $speciality->id }}">
                    </div>
                @endif
                <span class="badge badge-pill {{ $speciality->colour_class }}">{{ $speciality->speciality_name }}</span>
            @endforeach
        </div>
    </div>
</div>
