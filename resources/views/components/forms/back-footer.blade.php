<div class="card-footer">
    <div class="form-group row mb-0 justify-content-start">

        <div class="col-md-4 offset-md-0">
            <button type="button" class="btn btn-secondary">
                <a class="text-decoration-none text-light" href="{{ url($url) }}">@include('components.icons.left-icon'){{ __('Vissza') }}</a>
            </button>
        </div>

    </div>
</div>
