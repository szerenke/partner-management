<div class="modal fade" id="info-modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title text-center">{{ __('Saját adatok módosítása a profilban!') }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <form action="{{ route('users.index')}}" method="post">
                @csrf
                    <button type="button" class="btn btn-primary" data-dismiss="modal">
                        {{ __('Ok') }}
                    </button>
                </form>
            </div>

        </div>
    </div>
</div>
