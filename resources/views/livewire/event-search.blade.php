<div>
    <table class="table table-striped">
        <thead>
            <div class="form-row pt-3 pb-3">

                <div class="col-md-2">
                    <select wire:model="selectedMin" class="custom-select mr-sm-2">
                        @foreach($minYears as $minYear)
                            <option value="{{ $minYear }}">{{ $minYear }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-2">
                    <select wire:model="selectedMax" class="custom-select mr-sm-2">
                        @foreach($maxYears as $maxYear)
                            <option value="{{ $maxYear }}">{{ $maxYear }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-8 input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">@include('components.icons.search-icon')</div>
                    </div>
                 <input type="text" class="form-control" placeholder="Esemény neve..." wire:model="searchText">
                </div>
            </div>

            <tr>
                <th sortable class="col-md-3" scope="col">{{ __('Esemény') }}</th>
                <th class="col-md-4" scope="col">{{ __('Leírás') }}</th>
                <th class="col-md-4" scope="col">{{ __('Dátum') }}</th>
                <th class="small-td">{{ __('Résztvevők') }}</th>
                <th class="small-td">{{ __('Mutat') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($events as $event)
                <tr>
                    <th scope="row">{{ $event->event_name }} </th>
                    <td>{{ $event->event_description }}</td>
                    <td>{{ date('Y.m.d H:i', strtotime($event->start_date)) }} {{ __(' - ') }} {{ date('Y.m.d H:i', strtotime($event->end_date)) }} </td>

                    <td>
                        @foreach($event_participants as $num)
                            @if($num != null && $num['event_id'] == $event->id)
                                {{ $num['participants_no'] }}
                            @endif
                        @endforeach
                    </td>

                    <td><a href="{{ route('events.show',$event->id)}}" class="btn btn-success btn-show">
                            @include('components.icons.eye-icon')
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
</table>
    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            {{ $events->links() }}
        </div>
    </div>

</div>



