<div>
    <table class="table table-striped">
        <thead>
        <tr><th class="text-center" colspan="5"><h4>{{ __('Résztvevők meghívása') }}</h4></th></tr>

        <tr>
            <div class="d-flex form-row justify-content-center pt-3 pb-3">

                <div class="col-sm-2">
                    <select wire:model="position" class="custom-select mr-sm-2">
                        <option value="0">{{ __('Minden beosztás') }}</option>
                        <option value="1">{{ __('Vezető') }}</option>
                        <option value="2">{{ __('Beosztott') }}</option>
                    </select>
                </div>

                <div class="col-sm-2">
                    <select wire:model="selectedCategory"  class="custom-select mr-sm-2">
                        <option value="0">{{ __('Minden intézmény') }}</option>
                        @foreach($instituteCategories as $category)
                            <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-sm-1 text-center">
                    <label class="col-form-label text-md-right">{{ __('Szakterület:') }}</label>
                </div>
                <div class="col-md-5 pt-2 bg-light rounded pl-5">
                    <div class="form-check">
                        <div>
                            @foreach($specialities as $speciality)
                                <input checked wire:model="selectedSpecialities" class="form-check-input" type="checkbox" value="{{ $speciality->id }}" >
                                <label class="form-check-label" for="speciality{{ $speciality->id }}">
                                    <span class="badge badge-pill {{ $speciality->colour_class }}">{{ $speciality->speciality_name }}</span>
                                    <span class="text-light">{{ __('---') }}</span>
                                </label>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </tr>

        <tr>
            <th class="border-right">
                <div class="d-flex col-sm pl-1 justify-content-center border-secondary">
                    <input wire:click="clickAll" wire:model="checked" type="checkbox">
                </div>
            </th>
            <th class="col-md-1 text-center" scope="col">{{ __('ID') }}</th>
            <th class="col-md-3" scope="col">{{ __('Név') }}</th>
            <th class="col-md-2" scope="col">{{ __('Beosztás') }}</th>
            <th class="col-md-7" scope="col">{{ __('Intézmény') }}</th>
        </tr>
        </thead>
        <tbody>

        @csrf
        <div class="form-check">
            @foreach($people as $person)
                <tr>
                    <th class="border-right">
                        <div class="d-flex col-sm pl-4 justify-content-center ">
                            <input wire:click="unselected" {{ $checkedAll }} name="people[]" class="form-check-input" type="checkbox" value="{{ $person->id }}" id="person{{ $person->id }}">
                        </div>
                    </th>
                    <th scope="row" class="text-center">{{ $person->id }}</th>
                    <td>{{ $person->title }} {{ $person->lastname }} {{ $person->firstname }}</td>
                    <td>{{ $person->position }}</td>
                    <td>{{ $person->path }}</td>
                </tr>
            @endforeach
        </div>
        </tbody>
    </table>
</div>
