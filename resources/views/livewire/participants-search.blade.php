<div>
    <table class="table table-striped">
    <thead>
    <tr>
        <td colspan="5">
            <div class="bg-light rounded shadow">
                <div class="row">
                    <label class="col-md-2 col-form-label text-md-right">{{ __('Időpont:') }}</label>
                    <label class="col-md-5 col-form-label text-md-left">{{ date('Y.m.d H:i', strtotime($event->start_date)) }} {{ __(' - ') }} {{ date('Y.m.d H:i', strtotime($event->end_date)) }}</label>
                    <label class="col-md-2 col-form-label text-md-right">{{ __('Előadások:') }}</label>
                    <label class="col-md-1 col-form-label text-md-left"><a href="{{ route('presentations.show', $event->id) }}"><button class="btn btn-sm btn-light border-secondary rounded">{{ $countPresentations}}</button></a></label>
                </div>
                <div class="row">
                    <label class="col-md-2 col-form-label text-md-right">{{ __('Helyszín:') }}</label>
                    <label class="col-md-5 col-form-label text-md-left">{{ $event->event_location}}</label>
                    <label class="col-md-2 col-form-label text-md-right">{{ __('Esemény e-mailek:') }}</label>
                    <label class="col-md-1 col-form-label text-md-left"><a href="{{ route('eventemails.show', $event->id) }}"><button class="btn btn-sm btn-light border-secondary rounded">{{ $countEventEmails}}</button></a></label>

                </div>
                <div class="row pb-2">
                    <label class="col-md-2 col-form-label text-md-right">{{ __('Leírása:') }}</label>
                    <label class="col-md-8 col-form-label text-md-left">{{ $event->event_description}} </label>
                    @if(Auth::user()->hasRole('admin'))
                        @if($event->start_date > now())
                            <div class="col-sm-1">
                                <button class="btn btn-primary">
                                    <a class="text-light text-decoration-none" href="{{ route('events.edit', $event->id ) }}">@include('components.icons.edit-icon')</a>
                                </button>
                            </div>
                            <div class="col-sm-1">
                            @if($countParticipants === 0)
                                <!-- Button trigger modal -->
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $id = $event->id }}">
                                        @include('components.icons.delete-icon')
                                    </button>

                                    <!-- Modal -->
                                    @include('components.delede-modal', ['route' => 'events.destroy', 'headertext' => 'Esemény törlése',
                                              'bodytext' => 'Biztosan törölni akarja az eseményt minden adatával?' ])
                                @endif
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <th colspan="5">
            <div class="input-group row">
                <h3 class="col-md-3 text-center pl-5 pt-2">{{ __('Résztvevők') }}</h3>

                <div class="input-group col-md-7 mb-3 mr-sm-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">@include('components.icons.search-icon')</div>
                    </div>
                    <input type="text" class="form-control" placeholder="{{ __('Résztvevő neve...') }}" wire:model="searchText">
                </div>

                <div class="col">

                    @if(Auth::user()->hasRole('admin'))
                        @if($event->start_date > now())
                            <div class="col-sm-1">
                                <button class="btn btn-success btn-show">
                                    <a class="text-light text-decoration-none" href="{{ url('/participants/selectnew/options') }}">@include('components.icons.plus2-icon')</a>
                                </button>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </th>
    </tr>

    <tr class="thead-dark">
        <th class="col-md-1" scope="col">{{ __('ID') }}</th>
        <th class="col-md-3" scope="col">{{ __('Név') }}</th>
        <th class="col-md-2" scope="col">{{ __('Beosztás') }}</th>
        <th class="col-md-6" scope="col">{{ __('Intézmény') }}</th>
        @if(Auth::user()->hasRole('admin'))
            @if($event->start_date > now())
                <th class="col-md-1" scope="col">{{ __('Szerkeszt') }}</th>
            @endif
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($people as $person)
        <tr>
            <th scope="row">{{ $person->id }}</th>
            <td>{{ $person->title }} {{ $person->lastname }} {{ $person->firstname }}
                <br>
                @foreach($participant_roles as $participant)
                    @if($participant->person_id === $person->id && $participant->event_id === $event->id )
                        @foreach($participant->eventroles as $eventrole)
                            <span class="badge badge-pill {{ $eventrole->rolecolour_class }}">
                                {{ $eventrole->eventrole_name }}
                            </span>
                        @endforeach
                    @endif
                @endforeach
            </td>
            <td>{{ $person->position }}</td>
            <td>{{ $person->path }}
                <br>
                @if($person->institute_id != null)
                    @foreach($institutes_specialities as $institute)
                        @if($institute->id === $person->institute_id)
                            @foreach($institute->specialities as $speciality)
                                <span class="badge badge-pill {{ $speciality->colour_class }}">
                                                        {{ $speciality->speciality_name }}
                                                    </span>
                            @endforeach
                        @endif
                    @endforeach
                @endif
            </td>
            @if(Auth::user()->hasRole('admin'))
                @if($event->start_date > now())
                    <td>
                        <button class="btn btn-primary"><a href="{{ route('participants.edit', $person->id) }}" class="text-decoration-none text-light">@include('components.icons.edit-icon')</a></button>
                    </td>
                @endif
            @endif
        </tr>
    @endforeach

    </tbody>
    </table>
    <div class="row pb-2">
        <div class="col-md-4 offset-md-0">
            <button type="button" class="btn btn-secondary">
                <a class="text-decoration-none text-light" href="{{ url('events') }}">@include('components.icons.left-icon'){{ __('Vissza') }}</a>
            </button>
        </div>
        <div class="col-4 d-flex justify-content-center">
            {{$people->links()}}
        </div>
    </div>

    @if(session('status'))
        <div class="alert alert-danger">
            {{ session('status') }}
        </div>
    @endif
</div>
