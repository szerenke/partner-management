<div>
    <!--Select Option institute countries -->
        <div class="form-group row">
            <label for="country_id" class="col-md-4 col-form-label text-md-right">{{ __('Ország:') }}</label>
            <div class="col-md-6">
                <select wire:model="selectedCountry" name="country_id"  class="form-control">
                    <option value="" >{{ __('Ország') }}</option>
                    @foreach($countries as $country)
                        <option value="{{ $country->id }}" {{ old('country_id', $oldCountry) ==  $country->id ? 'selected' : ''}} >{{ $country->country_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

    <!--Select Option institute cities -->
    <div class="form-group row">
        <label for="city_id" class="col-md-4 col-form-label text-md-right">{{ __('Város:') }}</label>

        <div class="col-md-6">
            <select wire:model="selectedCity" name="city_id"  class="form-control">
                <option value="" >{{ __('Város') }}</option>
                @foreach($cities as $city)
                    <option value="{{ $city->id }}" {{ old('city_id', $oldCity) ==  $city->id ? 'selected' : ''}} >{{ $city->city_name }}</option>
                @endforeach
            </select>
        </div>
    </div>

        <!--Select Option institute addresses -->
    <div class="form-group row">
        <label for="address_id" class="col-md-4 col-form-label text-md-right">{{ __('Cím:') }}</label>

        <div class="col-md-6">
            <select name="address_id"  class="form-control">
                <option value="" >{{ __('Cím') }}</option>
                @foreach($addresses as $address)
                    <option value="{{ $address->id }}" {{ old('address_id', $oldAddress) ==  $address->id ? 'selected' : ''}} >{{ $address->postal_code }} {{ __(', ') }} {{ $address->address }}</option>
                @endforeach
            </select>
        </div>
    </div>

</div>
