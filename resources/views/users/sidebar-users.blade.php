<div class="sidebar d-flex min-vh-100 flex-column flex-shrink-0">
    <div class="menu">
        <ul class="my-nav navbar-nav">
            <li class="my-nav-item"><a class="menu-link side-a" href="{{ url('/users') }}"><strong>{{ __('Felhasználó lista') }}</strong></a></li>
            <li class="my-nav-item"><a class="menu-link side-a" href="{{ url('/users/create') }}"><strong>{{ __('Új felhasználó') }}</strong></a></li>
            <li class="my-nav-item"><a class="menu-link side-a" href="{{ url('/users/export/') }}"><strong>@include('components.icons.download-icon'){{ __('Export mind') }}</strong></a></li>
        </ul>
    </div>
</div>
