@extends('layouts.app')

@section('content')
    <div class="page">
        @include('users.sidebar-users')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Felhasználó szerkesztése') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <form method="POST" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data">
                                @csrf
                            <div class="card-body">

                                @include('components.forms.user-form')

                                <!--Select Option Role type-->
                                <div class="form-group row">
                                    <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('Jogosultság:') }}</label>

                                    <div class="col-md-6">
                                        <select name="role_id" class="form-select form-select-sm btn-sm btn-secondary">
                                            <option class="btn-light" value="user" {{ old('role_id', $role) == 'user' ? 'selected' : '' }}>{{ __('Felhasználó') }}</option>
                                            <option class="btn-light" value="admin" {{ old('role_id', $role) == 'admin' ? 'selected' : '' }}>{{ __('Adminisztrátor') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                                @include('components.forms.edit-footer')
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
