@extends('layouts.app')

@section('content')
    <div class="page">
        @include('users.sidebar-users')

        <div class="content pl-1 pr-1">
            <div Class=" welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Felhasználók') }}</h1></div>
            <div class="container user-table">

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">{{ __('ID') }}</th>
                        <th scope="col">{{ __('Vezetéknév') }}</th>
                        <th scope="col">{{ __('Keresztnév') }}</th>
                        <th scope="col">{{ __('E-mail') }}</th>
                        <th scope="col">{{ __('Jogosultság') }}</th>
                        @if(Auth::user()->hasRole('admin'))
                            <th class="small-td">{{ __('Szerkeszt') }}</th>
                            <th class="small-td">{{ __('Töröl') }}</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <th scope="row">{{ $id = $user->id }}</th>
                            <td>{{ $user->lastname }}</td>
                            <td>{{ $user->firstname }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->roles->first()->name }}</td>

                            @if(Auth::user()->hasRole('admin'))
                                @if($user->id !=  Auth::user()->id)
                                <td>
                                    <button class="btn btn-primary">
                                        <a class="text-decoration-none text-light" href="{{ route('users.edit',$user->id)}}" >
                                            @include('components.icons.edit-icon')
                                        </a>
                                    </button>
                                </td>

                                @elseif($user->id === Auth::user()->id)
                                <td>
                                    <!-- Button trigger modal -->
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#info-modal">
                                        @include('components.icons.edit-icon')
                                    </button>
                                    <!-- Modal -->
                                    @include('components.info-modal')
                                </td>
                                @endif

                                <td>
                                @if($user->id != Auth::user()->id)
                                    <!-- Button trigger modal -->
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $id }}">
                                        @include('components.icons.delete-icon')
                                    </button>

                                    <!-- Modal -->
                                    @include('components.delede-modal', ['route' => 'users.destroy', 'headertext' => 'Felhasználó törlése',
                                            'bodytext' => 'Biztosan törölni akarja a felhasználót minden adatával?' ])
                                @endif
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        {{$users->links()}}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
