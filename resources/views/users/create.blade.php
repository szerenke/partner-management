@extends('layouts.app')

@section('content')
    <div class="page">
        @include('users.sidebar-users')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Új felhasználó') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <form method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data">
                                @csrf
                            <div class="card-body">

                                @include('components.forms.user-form')

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Jelszó') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control rounded @error('password') is-invalid @enderror" name="password"autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Jelszó megerősítés') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control rounded" name="password_confirmation"autocomplete="new-password">
                                    </div>
                                </div>

                                <!--Select Option Role type-->
                                    <div class="form-group row">
                                        <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('Jogosultság:') }}</label>

                                        <div class="col-md-6">
                                        <select name="role_id"  class="form-select form-select-sm btn-sm btn-secondary">
                                            <option class="btn-light" value="user">{{ __('Felhasználó') }}</option>
                                            <option class="btn-light" value="admin">{{ __('Adminisztrátor') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                                @include('components.forms.edit-footer')

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
