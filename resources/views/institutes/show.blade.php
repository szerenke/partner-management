@extends('layouts.app')

@section('content')
    <div class="page">
        @include('people.sidebar-people')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Intézmény adatai') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">

                            <div class="card-header">
                                <h5 class="text-center">
                                    @for($i = count($parent_path)-1; $i >= 0; $i--)
                                        @if($i != 0)
                                            {{ $parent_path[$i]['institute_name'] }}
                                        <br>
                                        @endif
                                    @endfor
                                </h5>
                                <h3 class="text-center">{{ $institute->institute_name }}</h3>
                            </div>

                            <div class="card-body">

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label text-md-right">{{ __('Rövidítés:') }}</label>
                                    <label class="col-md-6 col-form-label text-md-left">{{ $institute->short_name }}</label>
                                    @if(Auth::user()->hasRole('admin'))
                                        <div class="col-md-3">
                                            <button class="btn btn-primary">
                                                <a class="text-light" href="{{ route('institutes.edit', $institute->id) }}">@include('components.icons.edit-icon')</a>
                                            </button>
                                        </div>
                                    @endif
                                </div>

                                <hr class="border-info">

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label text-md-right">{{ __('Cím:') }}</label>
                                    <label class="col-md-6 col-form-label text-md-left">
                                        @if($institute->address != null)
                                                {{ $institute->address->address }}
                                                <br>
                                                {{ $institute->address->city->city_name }}, {{ $institute->address->postal_code }}
                                                <br>
                                            @if($institute->address->city->country->country_name != 'Magyarország')
                                                {{ $institute->address->city->country->country_name }}
                                            @endif
                                        @endif
                                    </label>
                                    @if(Auth::user()->hasRole('admin'))
                                        <div class="col-md-3">
                                            <button class="btn btn-primary">
                                                <a class="text-light" href="{{ route('institutes.edit.address', $institute->id) }}">@include('components.icons.edit-icon')</a>
                                            </button>
                                        </div>
                                    @endif
                                </div>

                                <hr class="border-info">

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label text-md-right">{{ __('Kategória:') }}</label>
                                    <label class="col-md-6 col-form-label text-md-left">{{ $institute->category->category_name }}</label>

                                    @if(Auth::user()->hasRole('admin'))
                                        <div class="col-md-3">
                                            <button class="btn btn-primary">
                                                <a class="text-light" href="{{ route('institutes.edit.category', $institute->id) }}">@include('components.icons.edit-icon')</a>
                                            </button>
                                        </div>
                                    @endif
                                </div>

                                <hr class="border-info">

                                <div class="form-group row">
                                    <label for="phone" class="col-md-3 col-form-label text-md-right">{{ __('Szakterület:') }}</label>

                                    <div class="col-md-6">
                                        @foreach($specialities as $speciality)
                                            <span class="badge badge-pill {{ $speciality->colour_class }}">
                                                {{ $speciality->speciality_name }}
                                            </span>
                                        @endforeach
                                    </div>

                                    @if(Auth::user()->hasRole('admin'))
                                        <div class="col-md-3">
                                            <button class="btn btn-primary">
                                                <a class="text-light" href="{{ route('institutes.edit.specialities', $institute->id) }}">@include('components.icons.edit-icon')</a>
                                            </button>
                                        </div>
                                    @endif

                                </div>
                            </div>
                                @include('components.forms.back-footer', ['url' => $backUrl])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
