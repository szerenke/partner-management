@extends('layouts.app')

@section('content')
    <div class="page">
        @include('people.sidebar-people')
        <div class="content pl-1 pr-1">
            <div Class=" welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Intézmények') }}</h1></div>
            <div class="container user-table">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <table class="table">
                            @foreach($institutes as $institute)
                                <tr>
                                    <th class="col-md-10">
                                        <ul class="list-unstyled">
                                            @if(count($institute->childinstitutes))
                                                <li>{{$institute->institute_name}}
                                                    <button class="btn btn-sm btn-outline-secondary" type="button" data-toggle="collapse" data-target="#collapse{{$institute->id}}" aria-expanded="false" aria-controls="collapse{{$institute->id}}">
                                                        @include('components.icons.down-icon')
                                                    </button>

                                                </li>
                                                <div class="collapse" id="collapse{{$institute->id}}">
                                                    @include('institutes.childinstituteList',['childinstitutes' => $institute->childinstitutes])
                                                </div>
                                            @else
                                                <li>{{$institute->institute_name}}</li>
                                            @endif
                                                @foreach($institutes_specialities as $allinstitute)
                                                    @if($allinstitute->id == $institute->id)
                                                        @foreach($allinstitute->specialities as $speciality)
                                                            <span class="badge badge-pill {{ $speciality->colour_class }}">
                                                                {{ $speciality->speciality_name }}
                                                            </span>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                        </ul>
                                    </th>
                                    <td>
                                        <button class="btn btn-success btn-show">
                                            <a class="text-light" href="{{ route('institutes.show', $institute->id )}}">@include('components.icons.eye-icon')</a>
                                        </button>
                                    </td>
                                    @if($institute->childinstitutes()->count() === 0 && $institute->people()->count() === 0)
                                        @if(Auth::user()->hasRole('admin'))
                                        <td>
                                        <!-- Button trigger modal -->
                                            <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $id = $institute->id }}">
                                                @include('components.icons.delete-icon')
                                            </button>

                                            <!-- Modal -->
                                            @include('components.delede-modal', ['route' => 'institutes.destroy', 'headertext' => 'Intézmény törlése',
                                                    'bodytext' => 'Biztosan törölni akarja az intézményt minden adatával?' ])

                                        </td>
                                        @endif
                                    @endif
                                </tr>
                            @endforeach

                        </table>

                        <div class="row">
                            <div class="col-12 ">
                                {{$institutes->links()}}
                            </div>
                        </div>

                        @if(session('status'))
                            <div class="alert alert-danger">
                                {{ session('status') }}
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



