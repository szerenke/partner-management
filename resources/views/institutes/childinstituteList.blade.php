@foreach($childinstitutes as $childinstitute)
    <table class="table">
        <tr>
            <td class="col-md-10">
                <ul style="list-style-type:none;">
                    @if(count($childinstitute->childinstitutes))
                        <li>{{$childinstitute->institute_name}}  <button class="btn btn-sm btn-outline-secondary" type="button" data-toggle="collapse" data-target="#collapse{{$childinstitute->id}}" aria-expanded="false" aria-controls="collapse{{$childinstitute->id}}"> @include('components.icons.down-icon')</button></li>
                        <div class="collapse" id="collapse{{$childinstitute->id}}">
                            @include('institutes.childinstituteList',['childinstitutes' => $childinstitute->childinstitutes])
                        </div>
                    @else
                        <li>{{$childinstitute->institute_name}}</li>
                    @endif
                    @foreach($institutes_specialities as $allinstitute)
                        @if($allinstitute->id == $childinstitute->id)
                            @foreach($allinstitute->specialities as $speciality)
                                <span class="badge badge-pill {{ $speciality->colour_class }}">
                                    {{ $speciality->speciality_name }}
                                </span>
                            @endforeach
                        @endif
                    @endforeach
                </ul>
            </td>
            <td class="col-md-1">
                <button class="btn btn-success btn-show">
                    <a class="text-light" href="{{ route('institutes.show', $childinstitute->id )}}">@include('components.icons.eye-icon')</a>
                </button>
            </td>
            @if($childinstitute->childinstitutes()->count() === 0 && $childinstitute->people()->count() === 0)
                @if(Auth::user()->hasRole('admin'))
                <td class="col-md-1">
                    <!-- Button trigger modal -->
                    <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $id = $childinstitute->id }}">
                        @include('components.icons.delete-icon')
                    </button>

                    <!-- Modal -->
                    @include('components.delede-modal', ['route' => 'institutes.destroy', 'headertext' => 'Intézmény törlése',
                            'bodytext' => 'Biztosan törölni akarja az intézményt minden adatával?' ])

                </td>
                @endif
            @endif
        </tr>
    </table>
@endforeach





