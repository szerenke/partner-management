@extends('layouts.app')

@section('content')
    <div class="page">
        @include('people.sidebar-people')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Új partner') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">

                        <div class="card">
                            <div class="card-header">

                                <!-- Circles which indicates the steps of the form: -->
                                <div class="step-div">
                                    <span class="step step-finish"></span>
                                    <span class="step step-finish"></span>
                                    <span class="step step-finish"></span>
                                    <span class="step step-finish"></span>
                                    <span class="step step-finish"></span>
                                    <button type="button" class="close" aria-label="Close">
                                        <a class="text-secondary text-decoration-none" href="{{ route('people.index') }}"><span>&times;</span></a>
                                    </button>
                                </div>
                            </div>

                            <form method="POST" action="{{ route('institutes.for.person.selected', $person_id) }}" enctype="multipart/form-data">
                                @csrf
                            <div class="card-body">

                                @include('components.forms.select-institute')

                            </div>
                            <div class="card-footer">
                                <div class="form-group row mb-0 justify-content-center">

                                    <div class="col-md-4 offset-md-0">
                                        <button type="button" class="btn btn-secondary">
                                            <a class="text-decoration-none text-light" href="{{ url('categories/index/people/'.$person_id)}}">@include('components.icons.left-icon'){{ __('Vissza') }}</a>
                                        </button>
                                    </div>

                                    <div class="col-md-0 offset-md-4">
                                        <button type="submit" class="btn btn-primary">{{ __('Mentés') }}</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
