@extends('layouts.app')

@section('content')
    <div class="page">
        @include('people.sidebar-people')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Új intézmény') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <div class="card-header">

                                <!-- Circles which indicates the steps of the form: -->
                                <div class="step-div">
                                    <span class="step step-finish"></span>
                                    <span class="step step-finish"></span>
                                    <span class="step step-finish"></span>
                                    <span class="step"></span>
                                    <span class="step"></span>
                                    <button type="button" class="close" aria-label="Close">
                                        <a class="text-secondary text-decoration-none" href="{{ route('institutes.index') }}"><span>&times;</span></a>
                                    </button>
                                </div>
                            </div>

                            <form method="POST" action="{{ route('institutes.store') }}" enctype="multipart/form-data">
                                @csrf
                            <div class="card-body">
                                @include('components.forms.institute-form')
                            </div>
                                @include('components.forms.partner-steps-footer', ['url' => 'institutes/select/parent/back'])

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
