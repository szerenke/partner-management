@extends('layouts.app')

@section('content')
    <div class="page">
        @include('events.sidebar-events')

        <div class="content pl-1 pr-1">
            <div Class=" welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Események') }}</h1></div>
            <div class="container user-table">
                @livewire('event-search', ['event_participants' => $event_participants])
            </div>
        </div>
    </div>
@endsection
