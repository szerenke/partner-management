<div class="sidebar d-flex min-vh-100 flex-column flex-shrink-0">
    <div class="menu">
        <ul class="my-nav navbar-nav">
            <li class="my-nav-item"><a class="menu-link side-a" href="{{ url('/events') }}"><strong>{{ __('Események') }}</strong></a></li>
            @if(Auth::user()->hasRole('admin'))
                <li class="my-nav-item"><a class="menu-link side-a" href="{{ url('/events/create') }}"><strong>{{ __('Új esemény') }}</strong></a></li>
            @endif
            <li class="my-nav-item"><a class="menu-link side-a" href="{{ url('/events/all/export/') }}"><strong>@include('components.icons.download-icon'){{ __('Export mind') }}</strong></a></li>
        </ul>
    </div>
</div>
