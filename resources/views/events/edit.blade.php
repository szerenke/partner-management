@extends('layouts.app')

@section('content')
    <div class="page">
        @include('events.sidebar-events')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Esemény szerkesztése') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">
                        <div class="card">
                            <form method="POST" action="{{ route('events.update', $event->id) }}" enctype="multipart/form-data">
                                @csrf
                            <div class="card-body">
                                @include('components.forms.event-form')
                            </div>
                                @include('components.forms.edit-footer')
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
