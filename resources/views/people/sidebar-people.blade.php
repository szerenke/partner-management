<div class="sidebar d-flex min-vh-100 flex-column flex-shrink-0">
    <div class="menu">
        <ul class="my-nav navbar-nav">
            <li class="my-nav-item"><a class="menu-link side-a" href="{{ url('/people') }}"><strong>{{ __('Partnerek') }}</strong></a></li>
            @if(Auth::user()->hasRole('admin'))
                <li class="my-nav-item"><a class="menu-link side-a" href="{{ url('/people/create') }}"><strong>{{ __('Új partner') }}</strong></a></li>
            @endif
            <li class="my-nav-item"><a class="menu-link side-a" href="{{ url('/institutes') }}"><strong>{{ __('Intézmények') }}</strong></a></li>
            @if(Auth::user()->hasRole('admin'))
                <li class="my-nav-item"><a class="menu-link side-a" href="{{ route('categories.index') }}"><strong>{{ __('Új intézmény') }}</strong></a></li>
            @endif
            <li class="my-nav-item"><a class="menu-link side-a" href="{{ url('/partners/export/') }}"><strong>@include('components.icons.download-icon'){{ __('Export mind') }}</strong></a></li>
        </ul>
    </div>
</div>
