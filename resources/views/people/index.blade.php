@extends('layouts.app')

@section('content')
    <div class="page">
        @include('people.sidebar-people')

        <div class="content pl-1 pr-1">
            <div Class=" welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Partnerek') }}</h1></div>
            <div class="container user-table">

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="col-md-1" scope="col">{{ __('ID') }}
                            <a class="sort" href="{{ route('people.index') }}">@include('components.icons.up-icon')</a>
                            <a class="sort" href="{{ route('people.sort.desc') }}">@include('components.icons.down-icon')</a>
                        </th>
                        <th class="col-md-3" scope="col">{{ __('Név') }}
                            <a class="sort" href="{{ route('people.sort.name') }}">@include('components.icons.up-icon')</a>
                            <a class="sort" href="{{ route('people.sort.name.desc') }}">@include('components.icons.down-icon')</a>
                        </th>
                        <th class="col-md-2" scope="col">{{ __('Beosztás') }}
                            <a class="sort" href="{{ route('people.sort.position') }}">@include('components.icons.up-icon')</a>
                            <a class="sort" href="{{ route('people.sort.position.desc') }}">@include('components.icons.down-icon')</a>
                        </th>
                        <th class="col-md-7" scope="col">{{ __('Intézmény') }}
                            <a class="sort" href="{{ route('people.sort.institute')  }}">@include('components.icons.up-icon')</a>
                            <a class="sort" href="{{ route('people.sort.institute.desc') }}">@include('components.icons.down-icon')</a>
                        </th>
                        <th class="small-td">{{ __('Mutat') }}</th>
                        @if(Auth::user()->hasRole('admin'))
                            <th class="small-td">{{ __('Töröl') }}</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($people as $person)
                        <tr>
                            <th scope="row">{{ $id = $person->id }}</th>
                            <td>{{ $person->title }} {{ $person->lastname }} {{ $person->firstname }}</td>
                            <td>{{ $person->position }}</td>
                            <td>{{ $person->path }}
                                <br>
                                @if($person->institute_id != null)
                                    @foreach($institutes_specialities as $institute)
                                    @if($institute->id === $person->institute_id)
                                        @foreach($institute->specialities as $speciality)
                                        <span class="badge badge-pill {{ $speciality->colour_class }}">
                                                {{ $speciality->speciality_name }}
                                        </span>
                                            @endforeach
                                    @endif
                                    @endforeach
                                @endif
                            </td>
                            <td><a href="{{ route('people.show',$person->id)}}" class="btn btn-success btn-show">
                                    @include('components.icons.eye-icon')
                                </a>
                            </td>

                            @if(Auth::user()->hasRole('admin'))
                                <td>

                                    <!-- Button trigger modal -->
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $id }}">
                                        @include('components.icons.delete-icon')
                                    </button>

                                    <!-- Modal -->
                                    @include('components.delede-modal', ['route' => 'people.destroy', 'headertext' => 'Partner törlése',
                                              'bodytext' => 'Biztosan törölni akarja a partnert minden adatával?' ])

                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        {{$people->links()}}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
