@extends('layouts.app')

@section('content')
    <div class="page">
        @include('people.sidebar-people')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Partner szerkesztése') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <div class="card-header text-center">
                                <div><h5>{{ __('Alap adatok szerkesztése') }}</h5></div>
                            </div>
                            <form method="POST" action="{{ route('people.update', $person->id) }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">

                                @include('components.forms.person-form')
                                @include('components.forms.person-selection-edit')

                            </div>
                                @include('components.forms.edit-footer')
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
