@extends('layouts.app')

@section('content')
    <div class="page">
        @include('people.sidebar-people')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Partner adatai') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">

                            <div class="card-header">
                                <h3 class="text-center">{{ $person->title }} {{ $person->lastname }} {{ $person->firstname }}</h3>
                            </div>
                            <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">{{ __('Beosztás:') }}</label>
                                        <label class="col-md-6 col-form-label text-md-left">{{ $person->position }}</label>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">{{ __('Vezető:') }}</label>
                                        <label class="col-md-6 col-form-label text-md-left">{{ $manager }}</label>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">{{ __('Neme:') }}</label>
                                        <label class="col-md-6 col-form-label text-md-left">{{ $gender }}</label>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">{{ __('Leírás:') }}</label>
                                        <label class="col-md-6 col-form-label text-md-left">{{ $person->description  }}</label>

                                        @if(Auth::user()->hasRole('admin'))
                                        <div class="col-md-3">
                                            <button class="btn btn-primary">
                                                <a class="text-light" href="{{ route('people.edit', $person->id )}}">@include('components.icons.edit-icon')</a>
                                            </button>
                                        </div>
                                        @endif
                                    </div>

                                    <hr class="border-info">

                                    <div class="form-group row justify-content-end">
                                        <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('E-mail:') }}</label>
                                        <ul class="col-md-6 col-form-label text-md-left">
                                            @foreach($person->emails as $email)
                                                <li>{{ $email->email_address }}</li>
                                            @endforeach
                                        </ul>
                                        <div class="col-md-3">
                                        @if(Auth::user()->hasRole('admin'))
                                            <button class="btn btn-primary">
                                                <a class="text-light" href="{{ route('mails.edit', $person->id )}}">@include('components.icons.edit-icon')</a>
                                            </button>
                                        @endif
                                        </div>

                                    </div>

                                    <hr class="border-info">

                                    <div class="form-group row justify-content-end">
                                        <label for="phone" class="col-md-3 col-form-label text-md-right">{{ __('Telefon:') }}</label>
                                            <ul class="col-md-6 col-form-label text-md-left">
                                        @foreach($person->phones as $phone)
                                                <li>{{ $phone->phone_number }}</li>
                                        @endforeach
                                            </ul>

                                        <div class="col-md-3">
                                        @if(Auth::user()->hasRole('admin'))
                                            <button class="btn btn-primary">
                                                <a class="text-light" href="{{ route('phones.edit', $person->id )}}">@include('components.icons.edit-icon')</a>
                                            </button>
                                        @endif
                                        </div>
                                    </div>

                                <hr class="border-info">

                                @if($person->institute_id != null)
                                <div class="form-group row justify-content-end">
                                    <label class="col-md-3 col-form-label text-md-right">{{ __('Intézmény:') }}</label>
                                    <label class="col-md-5 col-form-label text-md-left">
                                        @for($i = count($parent_path)-1; $i >= 0; $i--)
                                            @if($i == 0)
                                                <strong>{{ $parent_path[$i]['institute_name'] }}</strong>
                                            @else
                                                {{ $parent_path[$i]['institute_name'] }}
                                            @endif
                                            <br>
                                        @endfor
                                    </label>

                                    <div class="col-md-4">
                                        <button class="btn  btn-success btn-show">
                                            <a class="text-light" href="{{ route('institutes.show', $person->institute_id )}}">@include('components.icons.eye-icon')</a>
                                        </button>
                                        @if(Auth::user()->hasRole('admin'))
                                            <button class="btn btn-primary ">
                                                <a class="text-decoration-none text-light" href="{{ url('categories/edit/people/'.$person->id) }}">@include('components.icons.edit-icon')</a>
                                            </button>
                                        @endif
                                    </div>

                                </div>
                                <div class="form-group row justify-content-center">
                                    @foreach($specialities as $speciality)
                                        <span class="badge badge-pill {{ $speciality->colour_class }}">
                                            {{ $speciality->speciality_name }}
                                        </span>
                                    @endforeach
                                </div>

                                @else
                                <div class="form-group row justify-content-center">
                                    @if(Auth::user()->hasRole('admin'))
                                        <button class="btn btn-primary">
                                            <a class="text-decoration-none text-light" href="{{ url('categories/edit/people/'.$person->id) }}">{{ __('Intézmény hozzáadása') }}</a>
                                        </button>
                                    @endif
                                </div>
                                @endif

                            </div>
                                @include('components.forms.back-footer', ['url' => 'people'])
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
