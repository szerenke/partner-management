@extends('layouts.app')

@section('content')
    <div class="page">
        @include('people.sidebar-people')

        <div class="content pl-1 pr-1">
            <div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Új partner') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <div class="card-header">

                                <!-- Circles which indicates the steps of the form: -->
                                <div class="step-div">
                                    <span class="step step-finish"></span>
                                    <span class="step"></span>
                                    <span class="step"></span>
                                    <span class="step"></span>
                                    <span class="step"></span>
                                    <button type="button" class="close" aria-label="Close">
                                        <a class="text-secondary text-decoration-none" href="{{ route('people.index') }}"><span>&times;</span></a>
                                    </button>
                                </div>
                            </div>

                            <form method="POST" action="{{ route('people.store') }}" enctype="multipart/form-data">
                                @csrf
                            <div class="card-body">

                                @include('components.forms.person-form')

                                @if(request()->session()->get('person_id') != null)
                                    @include('components.forms.person-selection-edit')
                                @else
                                    <!--Select Option manager -->
                                    <div class="form-group row">
                                        <label for="manager_id" class="col-md-4 col-form-label text-md-right">{{ __('Vezető:') }}</label>

                                        <div class="col-md-6">
                                            <select name="manager_id"  class="form-select form-select-sm btn-sm">
                                                <option value="0">{{ __('Nem') }}</option>
                                                <option value="1">{{ __('Igen') }}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!--Select Option gender type-->
                                    <div class="form-group row">
                                        <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Neme:') }}</label>

                                        <div class="col-md-6">
                                            <select name="gender" class="form-select form-select-sm btn-sm">
                                                <option value="male">{{ __('Férfi') }}</option>
                                                <option value="female">{{ __('Nő') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                @endif

                            </div>
                                @include('components.forms.partner-steps-footer', ['url' => '/people'])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
