<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Partner nyilvántartó</title>

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="antialiased">

    <div class="container-md">
        <div class="pt-5 pb-5"><img src="/picture/mik_logo_white.png" alt="MIK_logo" class="mik-logo"></div>
        <div class="pb-5 d-flex justify-content-center"><h1 class="welcome">{{ __('Partner nyilvántartó') }}</h1></div>

        <div class="d-flex justify-content-center">
            @if (Route::has('login'))
                <div>
                    @auth
                        <a href="{{ url('/dashboard') }}" class="btn btn-light btn-login"><strong>{{ __('Vissza a főoldalra') }}</strong></a>
                    @else
                        <a href="{{ route('login') }}" class="btn btn-lg btn-light btn-login"><strong>{{ __('Bejelentkezem') }}</strong></a>
                    @endauth
                </div>
            @endif
        </div>
    </div>
    @include('layouts.footer')
    </body>
</html>
