@extends('layouts.app')

@section('content')
    <div class="page">
        @include('people.sidebar-people')

        <div class="content pl-1 pr-1">
            <<div Class="welcome pt-4 pb-4 d-flex justify-content-center"><h1>{{ __('Intézmény szerkesztése') }}</h1></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="text-center">{{ $institute->institute_name }}</h3>
                            </div>

                            <form method="POST" action="{{ route('institutes.update.specialities', $institute->id) }}" enctype="multipart/form-data">
                                @csrf
                            <div class="card-body">
                                @include('components.forms.select-institute-specialities')
                            </div>
                                @include('components.forms.edit-footer')
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
