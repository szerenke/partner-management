<?php

namespace Database\Seeders;

use App\Models\Institute;
use App\Models\Speciality;
use Illuminate\Database\Seeder;

class InstitutesSpecialitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $specialities = Speciality::all();

        Institute::where('category_id', '!=', 6)->each(function ($institute) use ($specialities){
            $institute->specialities()->attach(
                $specialities->random(rand(1, 3))->pluck('id')
            );
        });
    }
}
