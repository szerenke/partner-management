<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class RolesUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::all();

        User::whereNotIn('id', [11, 12])->each(function ($user) use ($roles){
            $user->roles()->attach(
                $roles->random(1)->pluck('id')
            );
        });

        User::where('id', '=', 11)->first()->roles()->attach(1);
        User::where('id', '=', 12)->first()->roles()->attach(2);
    }
}
