<?php

namespace Database\Seeders;

use App\Models\Event;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::factory()->times(10)->create();

        DB::table('events')->insert([
            [
                'event_name' => 'MIK Partners 2022',
                'event_description' => 'Szakmai napok a PTE MIK céges partnereinek és konferencia.',
                'event_location' => '7624 Pécs, Boszorkány u. 2.',
                'start_date' => '2022-05-15 09:00:00',
                'end_date' => '2022-05-15 23:00:00',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'event_name' => 'Pollack Expo 2022',
                'event_description' => 'Előadás sorozat és szakmai kiállítás.',
                'event_location' => '7624 Pécs, Boszorkány u. 2.',
                'start_date' => '2022-02-24 09:00:00',
                'end_date' => '2022-02-25 14:00:00',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
