<?php

namespace Database\Seeders;

use App\Models\Email;
use App\Models\Phone;
use Illuminate\Database\Seeder;
use App\Models\Person;

class PeopleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Person::factory()->times(40)->create();
    }
}
