<?php

namespace Database\Seeders;

use App\Models\Address;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AddressesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Address::factory()->times(20)->create();

        DB::table('addresses')->insert([
            [
                'postal_code' => 'H-7622',
                'address' => 'Vasvári Pál u. 4.',
                'city_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'postal_code' => 'H-7633',
                'address' => 'Szántó Kovács János u. 1/B',
                'city_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'postal_code' => 'H-7624',
                'address' => 'Boszorkány út 2.',
                'city_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'postal_code' => 'CH 123423453',
                'address' => '30 China str.',
                'city_id' => 4,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'postal_code' => 'CO 80204',
                'address' => '890 Auraria Pkwy',
                'city_id' => 3,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'postal_code' => 'H-7632',
                'address' => 'Iskola utca 2.',
                'city_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'postal_code' => 'H-7632',
                'address' => 'Iskola utca 30.',
                'city_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
