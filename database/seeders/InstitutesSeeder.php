<?php

namespace Database\Seeders;

use App\Models\Institute;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InstitutesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Institute::factory()->times(25)->create();

        DB::table('institutes')->insert([
            [
                'address_id' => 21,
                'category_id' => 2,
                'institute_name' => 'Pécsi Tudományegyetem',
                'parent_id' => null,
                'short_name' => 'PTE',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'address_id' => 21,
                'category_id' => 2,
                'institute_name' => 'Nemzetköziesítési Igazgatóság',
                'parent_id' => 26,
                'short_name' => 'NI',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'address_id' => 22,
                'category_id' => 2,
                'institute_name' => 'Kancellária',
                'parent_id' => 26,
                'short_name' => 'KA',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'address_id' => 22,
                'category_id' => 2,
                'institute_name' => 'Informatikai Főigazgatóság',
                'parent_id' => 28,
                'short_name' => 'IFI',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'address_id' => 23,
                'category_id' => 2,
                'institute_name' => 'Műszaki és Informatikai Kar',
                'parent_id' => 26,
                'short_name' => 'MIK',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'address_id' => 23,
                'category_id' => 2,
                'institute_name' => 'Informatikai és Villamos Intézet',
                'parent_id' => 30,
                'short_name' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'address_id' => 23,
                'category_id' => 2,
                'institute_name' => 'Építész Szakmai Intézet',
                'parent_id' => 30,
                'short_name' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'address_id' => 24,
                'category_id' => 2,
                'institute_name' => 'Whuan University',
                'parent_id' => null,
                'short_name' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'address_id' => 25,
                'category_id' => 2,
                'institute_name' => 'Metropolitan State University of Denver',
                'parent_id' => null,
                'short_name' => 'MSU Denver',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'address_id' => 26,
                'category_id' => 3,
                'institute_name' => 'Pécsi Középiskola',
                'parent_id' => null,
                'short_name' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'address_id' => 27,
                'category_id' => 3,
                'institute_name' => 'Pécsi Szakközépiskola',
                'parent_id' => null,
                'short_name' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'address_id' => 10,
                'category_id' => 5,
                'institute_name' => 'Magyar Iparkamara',
                'parent_id' => null,
                'short_name' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'address_id' => null,
                'category_id' => 6,
                'institute_name' => 'Magánszemély',
                'parent_id' => null,
                'short_name' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
