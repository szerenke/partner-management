<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'category_name' => 'Cég',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'category_name' => 'Egyetem',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'category_name' => 'Középiskola',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'category_name' => 'Egyéni vállalkozó',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'category_name' => 'Egyéb',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'category_name' => 'Magánszemély',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
