<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            [
                'country_name' => 'Magyarország',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'country_name' => 'USA',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'country_name' => 'Kína',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
