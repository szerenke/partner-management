<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventemailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('eventemails')->insert([
            [
                'sender' => 'partners@mik.hu',
                'email_subject' => 'MIK Partners Meghívó',
                'email_message' => 'Szeretettel meghívjuk a MIK Partners rendezvényünkre.',
                'signature' => 'A Kar Vezetősége',
                'event_link' => 'https://mik.pte.hu/mik-partners-szakmai-nap-1',
                'event_id' => 11,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'sender' => 'expo@mik.hu',
                'email_subject' => 'Pollack EXPO Meghívó',
                'email_message' => 'Szeretettel meghívjuk a Pollack EXPO kiállításra és konferenciára.',
                'signature' => 'Dékáni Vezetés',
                'event_link' => 'https://pollackexpo.mik.pte.hu/',
                'event_id' => 12,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
