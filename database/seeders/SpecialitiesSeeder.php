<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SpecialitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('specialities')->insert([
            [
                'speciality_name' => 'Építész',
                'colour_class' => 'badge-dark',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'speciality_name' => 'Informatika',
                'colour_class' => 'badge-primary',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'speciality_name' => 'Gépész',
                'colour_class' => 'badge-secondary',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'speciality_name' => 'Villamos',
                'colour_class' => 'badge-danger',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
