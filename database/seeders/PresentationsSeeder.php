<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PresentationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $eventroles_participants = DB::table('participants')
            ->join('eventrole_participant', 'participants.id', "=", 'eventrole_participant.participant_id')
            ->join('eventroles', 'eventroles.id', '=', 'eventrole_participant.eventrole_id')
            ->where('eventroles.id', '=', 3)
            ->select('participants.person_id as person_id', 'participants.event_id as event_id')
            ->get();

        foreach ($eventroles_participants as $participant){
            DB::table('presentations')->insert([
                'presentation_title' => Str::random(15),
                'presentation_description' => Str::random(25),
                'event_id' => $participant->event_id,
                'person_id' => $participant->person_id,
                'speciality_id' => rand(1, 3),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
