<?php

namespace Database\Seeders;

use App\Models\Eventrole;
use App\Models\Participant;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LaratrustSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(RolesUsersSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(AddressesSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(InstitutesSeeder::class);
        $this->call(PeopleSeeder::class);
        $this->call(EmailsSeeder::class);
        $this->call(PhonesSeeder::class);
        $this->call(SpecialitiesSeeder::class);
        $this->call(InstitutesSpecialitiesSeeder::class);
        $this->call(EventsSeeder::class);
        $this->call(ParticipantsSeeder::class);
        $this->call(EventrolesSeeder::class);
        $this->call(EventemailsSeeder::class);
        $this->call(EventrolesParticipantsSeeder::class);
        $this->call(PresentationsSeeder::class);
    }
}
