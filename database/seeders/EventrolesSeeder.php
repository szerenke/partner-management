<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventrolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('eventroles')->insert([
            [
                'eventrole_name' => 'Meghívott',
                'rolecolour_class' => 'badge-info',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'eventrole_name' => 'Eljön',
                'rolecolour_class' => 'badge-success',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'eventrole_name' => 'Előadó',
                'rolecolour_class' => 'badge-warning',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
