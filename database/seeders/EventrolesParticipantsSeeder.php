<?php

namespace Database\Seeders;

use App\Models\Participant;
use Database\Factories\EventrolesParticipantsFactory;
use Illuminate\Database\Seeder;

class EventrolesParticipantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $participants = Participant::all();

        foreach ($participants as $participant){
            if($participant->id % 9 != 0){
                $participant->eventroles()->attach(1);
            }
            if($participant->id % 4 != 0){
                $participant->eventroles()->attach(2);
            }
            if($participant->id % 10 === 0 && $participant->eventroles()->where('eventrole_id', 2)){
                $participant->eventroles()->attach(3);
            }
        }
    }
}
