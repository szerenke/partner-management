<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            [
                'city_name' => 'Pécs',
                'country_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'city_name' => 'Budapest',
                'country_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'city_name' => 'Denver',
                'country_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'city_name' => 'Whuan',
                'country_id' => 3,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
