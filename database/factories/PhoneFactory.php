<?php

namespace Database\Factories;

use Faker\Factory as Faker;
use App\Models\Person;
use App\Models\Phone;
use Illuminate\Database\Eloquent\Factories\Factory;

class PhoneFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Phone::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    private static $num = 1;
    private static $person = 1;
    public function definition()
    {
        $faker = Faker::create('hu_HU');

        $increment = self::$num++;
        if($increment % 2 === 0){
            $person_id = self::$person++;
        } else {
            $person_id = self::$person;
        }

        return [
            'phone_number' => $faker->phoneNumber,
            'person_id' => $person_id
        ];
    }
}
