<?php

namespace Database\Factories;

use App\Models\Email;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Email::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    private static $num = 1;
    private static $person = 1;
    public function definition()
    {
        $increment = self::$num++;
        if($increment % 2 == 0){
            $person_id = self::$person++;
        } else {
            $person_id = self::$person;
        }

        return [
            'email_address' => $this->faker->unique()->safeEmail(),
            'person_id' => $person_id
        ];
    }
}
