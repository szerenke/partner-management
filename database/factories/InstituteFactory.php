<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\Institute;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class InstituteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Institute::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    private static $id = 0;
    public function definition()
    {
        $max_id = self::$id++;
        $ran = null;

        if(self::$id % 3 == 0)
        {
            $ran = $this->faker->numberBetween(1, $max_id);
        }

        $max_id = count(Address::all());
        $address_id = $this->faker->numberBetween(1, $max_id);

        $company_name = $this->faker->company;
        $short = preg_replace("/(?![A-Z])./", "", $company_name);

        return [
            'address_id' => $address_id,
            'category_id' => 1,
            'institute_name' => $company_name,
            'short_name' => $short,
            'parent_id' => $ran != self::$id ? $ran : null
        ];
    }
}
