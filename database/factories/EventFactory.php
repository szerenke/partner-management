<?php

namespace Database\Factories;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        $faker = Faker::create('hu_HU');
        $startDate = $this->faker->dateTimeInInterval('-8 years', '+7 years');
        $endDate = $this->faker->dateTimeInInterval($startDate, '+2 days');

        $event = $this->faker->randomElements(['Nyílt nap', 'Nyílt nap', 'Nyílt nap', 'MIK Partners', 'Pollack Expo', 'PhD-DLA Symposium', 'Kutatók éjszakája'])[0];
        $year = $startDate->format('Y');
        $event_year = $event . ' ' . $year;

        return [
            'event_name' => $event_year,
            'event_description' => $this->faker->text(40),
            'event_location' => $faker->address,
            'start_date' => $startDate,
            'end_date' => $endDate
        ];
    }
}
