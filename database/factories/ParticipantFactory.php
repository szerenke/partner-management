<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ParticipantFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    private static $event_id = 1;
    private static $person_id = 0;
    private static $num = 0;

    public function definition()
    {
        self::$person_id++;
        self::$num++;

        if(self::$num % 10 === 0)
        {
            self::$event_id++;
            self::$person_id = 1;
        }

        if(self::$num % 10 === 0 && self::$num % 20 != 0 && self::$num % 30 != 0)
        {
            self::$person_id = 10;
        } else if(self::$num % 20 === 0)
        {
            self::$person_id = 20;
        } else if(self::$num % 20 === 0) {
            self::$person_id = 30;
        }

        return [
            'person_id' => self::$person_id,
            'event_id' => self::$event_id
        ];
    }
}
