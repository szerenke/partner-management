<?php

namespace Database\Factories;

use App\Models\Institute;
use Faker\Factory as Faker;
use App\Models\Person;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Person::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

    private static $id = 1;
    public function definition()
    {
        $faker = Faker::create('hu_HU');
        $gender = $this->faker->randomElements(['male', 'female'])[0];
        $position = $this->faker->randomElements(['vezető', 'vezetőhelyettes', 'menedzser', 'titkár', 'beosztott',
                                                    'asszisztens', 'ügyvivő szakértő'])[0];

        $max_id = count(Institute::all());
        $institute_id = $this->faker->numberBetween(1, $max_id);

        $manager_id = self::$id++;

        return [
            'firstname' => $faker->firstName($gender),
            'lastname' => $faker->lastName,
            'title' => $this->faker->title($gender),
            'position' => $position,
            'gender'=> $gender,
            'description' => $this->faker->text(40),
            'institute_id' => $institute_id,
            'manager_id' => $position == 'vezető' ? $manager_id : ($position == 'vezetőhelyettes' ? $manager_id : null)
        ];
    }
}
