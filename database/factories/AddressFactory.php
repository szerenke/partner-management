<?php

namespace Database\Factories;

use App\Models\Address;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = Faker::create('hu_HU');
        $city_id = $this->faker->numberBetween(1, 2);

        return [
            'postal_code' => $faker->postcode,
            'address' => $faker->streetAddress,
            'city_id' => $city_id
        ];
    }
}
