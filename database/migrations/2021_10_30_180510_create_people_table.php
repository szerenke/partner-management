<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();
            $table->string('lastname');
            $table->string('firstname');
            $table->string('title')->nullable();
            $table->string('position')->nullable();
            $table->enum('gender', ['male', 'female']);
            $table->text('description')->nullable();

            $table->foreignId('institute_id')
                ->nullable()
                ->constrained('institutes')
                ->onDelete('set null');

            $table->foreignId('manager_id')
                ->nullable()
                ->constrained('people')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
