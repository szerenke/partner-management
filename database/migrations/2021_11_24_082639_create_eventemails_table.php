<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventemailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventemails', function (Blueprint $table) {
            $table->id();
            $table->string('sender');
            $table->string('email_subject');
            $table->text('email_message');
            $table->text('signature');
            $table->string('event_link')->nullable(); //use here or move it to events
            $table->dateTime('sent_date')->nullable();//may have not needed
            $table->foreignId('event_id')
                ->constrained()
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventemails');
    }
}
