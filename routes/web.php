<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// for admins
Route::group(['middleware' => ['auth', 'role:admin']], function() {

    Route::get('/users/export', 'App\Http\Controllers\UsersExportController@export');
    Route::resource('users', 'App\Http\Controllers\UsersController');
    Route::post('/users/{user}', 'App\Http\Controllers\UsersController@update');

    Route::resource('people', 'App\Http\Controllers\PeopleController');
    Route::post('/people/{person}', 'App\Http\Controllers\PeopleController@update');

    Route::resource('mails', 'App\Http\Controllers\EmailsController');
    Route::post('/mails/{email}', 'App\Http\Controllers\EmailsController@update');

    Route::resource('phones', 'App\Http\Controllers\PhonesController');
    Route::post('/phones/{phone}', 'App\Http\Controllers\PhonesController@update');

    Route::resource('institutes', 'App\Http\Controllers\InstitutesController');
    Route::post('institutes/{institute}', 'App\Http\Controllers\InstitutesController@update');

    Route::get('institutes/{institute}/edit/category', 'App\Http\Controllers\InstitutesController@editCategory')->name('institutes.edit.category');
    Route::post('institutes/{institute}/update/category', 'App\Http\Controllers\InstitutesController@updateCategory')->name('institutes.update.category');

    Route::get('institutes/{institute}/edit/specialities', 'App\Http\Controllers\InstitutesController@editSpecialities')->name('institutes.edit.specialities');
    Route::post('institutes/{institute}/update/specialities', 'App\Http\Controllers\InstitutesController@updateSpecialities')->name('institutes.update.specialities');

    Route::post('institutes/for/person/{person}', 'App\Http\Controllers\InstitutesController@categoryselectedPerson')->name('institutes.for.person'); //person
    Route::post('institutes/for/person/selected/{person}', 'App\Http\Controllers\InstitutesController@selectedInstitutePerson')->name('institutes.for.person.selected'); //person
    Route::get('institutes/edit/address/{institute}', 'App\Http\Controllers\InstitutesController@editAddress')->name('institutes.edit.address');
    Route::post('institutes/{institute}/update/address', 'App\Http\Controllers\InstitutesController@updateAddress')->name('institutes.update.address');

    Route::post('institutes/category/selected', 'App\Http\Services\InstitutesService@instituteCategorySelected')->name('institutes.category.selected'); //new institute
    Route::post('institutes/select/parent', 'App\Http\Services\InstitutesService@selectParentInstitute')->name('institutes.select.parent'); //new institute
    Route::get('institutes/select/parent/back', 'App\Http\Services\InstitutesService@backSelectedParentInstitute')->name('institutes.select.parent.back'); //new institute
    Route::post('institutes/save/address', 'App\Http\Controllers\InstitutesController@saveAddress')->name('institutes.save.address'); //new institute
    Route::post('institutes/save/specialities', 'App\Http\Controllers\InstitutesController@saveSpecialities')->name('institutes.save.specialities'); //new institute

    Route::resource('categories', 'App\Http\Controllers\CategoriesController');
    Route::get('categories/index/people/{person}', 'App\Http\Controllers\CategoriesController@indexPeople')->name('categories.index.people'); //person
    Route::get('categories/edit/people/{person}', 'App\Http\Controllers\CategoriesController@editPeople')->name('categories.edit.people'); //person

    Route::resource('countries', 'App\Http\Controllers\CountriesController');
    Route::resource('cities', 'App\Http\Controllers\CitiesController');
    Route::resource('addresses', 'App\Http\Controllers\AddressesController');
    Route::resource('specialities', 'App\Http\Controllers\SpecialitiesController');

    Route::resource('events', 'App\Http\Controllers\EventsController');
    Route::post('events/{event}', 'App\Http\Controllers\EventsController@update');
    Route::get('events/send/invitation/{$id}', 'App\Http\Controllers\EventsController@sendEventMail')->name('events.send.invitation');
    Route::post('events/email/sent', 'App\Http\Controllers\EventsController@eventMailSent')->name('events.email.sent');

    Route::resource('participants', 'App\Http\Controllers\ParticipantsController');
    Route::post('participants/update/roles', 'App\Http\Controllers\ParticipantsController@updateRoles')->name('participant.update.roles');
    Route::get('participants/selectnew/options', 'App\Http\Services\EventsService@addParticipantsOption')->name('participants.selectnew.options');
    Route::post('participants/select/event/email', 'App\Http\Services\EventsService@selectEventEmail')->name('participants.select.event.email');
    Route::post('participants/eventemail/selected', 'App\Http\Services\EventsService@eventEmailSelected')->name('participants.eventemail.selected');

    Route::resource('eventemails', 'App\Http\Controllers\EventemailsController');
    Route::post('eventemails/{eventemail}', 'App\Http\Controllers\EventemailsController@update');

    Route::resource('presentations', 'App\Http\Controllers\PresentationsController');
    Route::post('presentations/{presentation}', 'App\Http\Controllers\PresentationsController@update');
});

//auth route for both
Route::group(['middleware' => ['auth']], function() {
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');
    Route::get('/dashboard/profile/myprofile', 'App\Http\Controllers\DashboardController@myprofile')->name('dashboard.myprofile');

    Route::get('/profile/{user}/edit', 'App\Http\Controllers\ProfileController@edit')->name('profile.edit');
    Route::post('/profile/{user}', 'App\Http\Controllers\ProfileController@update')->name('profile.update');
    Route::get('/profile/{user}/edit-password', 'App\Http\Controllers\ProfileController@editPassword')->name('profile.edit-password');
    Route::post('/profile/{user}/update-password', 'App\Http\Controllers\ProfileController@updatePassword')->name('profile.update-password');

    Route::get('/partners/export', 'App\Http\Controllers\PartnersExportController@export');
    Route::resource('people', 'App\Http\Controllers\PeopleController')->only('index', 'show');
    Route::get('/people/sort/desc', 'App\Http\Controllers\PeopleController@indexDesc')->name('people.sort.desc');
    Route::get('/people/sort/institute', 'App\Http\Controllers\PeopleController@sortByInstitute')->name('people.sort.institute');
    Route::get('/people/sort/institute/desc', 'App\Http\Controllers\PeopleController@sortByInstituteDesc')->name('people.sort.institute.desc');
    Route::get('/people/sort/name', 'App\Http\Controllers\PeopleController@sortByLastname')->name('people.sort.name');
    Route::get('/people/sort/name/desc', 'App\Http\Controllers\PeopleController@sortByLastnameDesc')->name('people.sort.name.desc');
    Route::get('/people/sort/position', 'App\Http\Controllers\PeopleController@sortByPosition')->name('people.sort.position');
    Route::get('/people/sort/position/desc', 'App\Http\Controllers\PeopleController@sortByPositionDesc')->name('people.sort.position.desc');

    Route::resource('institutes', 'App\Http\Controllers\InstitutesController')->only('index', 'show');

    Route::resource('events', 'App\Http\Controllers\EventsController')->only('index', 'show');
    Route::get('/events/all/export', 'App\Http\Controllers\EventsExportController@export');

    Route::resource('eventemails', 'App\Http\Controllers\EventemailsController')->only('show');
    Route::resource('presentations', 'App\Http\Controllers\PresentationsController')->only('show');
});

require __DIR__.'/auth.php';
